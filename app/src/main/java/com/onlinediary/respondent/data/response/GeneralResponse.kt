package com.onlinediary.respondent.data.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GeneralResponse(@SerializedName("success")
                           @Expose
                           val success: Boolean?) : Serializable