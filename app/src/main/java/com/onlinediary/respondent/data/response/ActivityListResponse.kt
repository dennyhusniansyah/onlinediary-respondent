package com.onlinediary.respondent.data.response

import com.onlinediary.respondent.data.repository.local.entity.ActivityList


class ActivityListResponse : ArrayList<ActivityList>()