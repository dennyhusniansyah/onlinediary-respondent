package com.onlinediary.respondent.data.repository.local.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Timing(
    @SerializedName("end_time")
    val endTime: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("start_time")
    val startTime: String?
):Parcelable