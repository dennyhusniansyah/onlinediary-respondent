package com.onlinediary.respondent.data.repository.local.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CommentItems(
    @SerializedName("activities_id")
    val activitiesId: String,
    @SerializedName("bots_id")
    val botsId: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("photo")
    val photo: String?,
    @SerializedName("post")
    val post: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("users_id")
    val usersId: String
):Parcelable
