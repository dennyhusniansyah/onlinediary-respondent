package com.onlinediary.respondent.data.repository.local

import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.data.response.ProfileResponse
import com.onlinediary.respondent.utils.DateTimeHelper.getTodayDateTime

class LocalServiceManager(private val preferenceService: PreferenceService) {

    companion object {
        const val SESSION = "local.preferences.session"
        const val PROFILE_CREDENTIALS = "local.preferences.credentials.profile"
        const val KOMODITAS = "local.preferences.KOMODITAS"
        const val TOKEN_CREDENTIALS = "local.preferences.credentials.token"
        const val SUBSCRIPTION_DATE = "local.preferences.credentials.subscription"

        const val DATE_FOR_SAVE = "local.preferences.date"
        const val VERSION_FOR_CHECK = "local.preferences.data.version"

        const val ISNAIK = "local.preferences.data.isnaik"
    }




    fun isSessionActive(): Boolean {
        return preferenceService.getBoolean(SESSION, false)
    }

    fun startSession() {
        return preferenceService.putBoolean(SESSION, true)
    }

    fun removeSession() {
        return preferenceService.removePreference(SESSION)
    }

    fun saveProfileCredentials(dataToSave: ProfileResponse) {
        preferenceService.putObject(PROFILE_CREDENTIALS, dataToSave)
    }


    fun getProfileCredentials(): ProfileResponse {
        return preferenceService.getObject(PROFILE_CREDENTIALS,
            ProfileResponse.empty(),
            ProfileResponse::class.java)
    }

    fun removeProfileCredentials() {
        return preferenceService.removePreference(PROFILE_CREDENTIALS)
    }


    fun saveAccessTokenCredentials(accessToken: String?) {
        accessToken?.let { preferenceService.putString(TOKEN_CREDENTIALS, it, true) }
    }

    fun removeAccessTokenCredentials() {
        return preferenceService.removePreference(TOKEN_CREDENTIALS)
    }

    fun getAccessTokenCredentials(): String? {
        return preferenceService.getString(TOKEN_CREDENTIALS, "", true)
    }

    fun saveSubscription(subscription: String){
        preferenceService.putString(SUBSCRIPTION_DATE,subscription,false)
    }

    fun getSubscription(): String?{
        return preferenceService.getString(SUBSCRIPTION_DATE,"", false)
    }

    fun removeSubscription(){
        return preferenceService.removePreference(SUBSCRIPTION_DATE)
    }

    fun getSavedDayForCheck(): String? {
        return preferenceService.getString(DATE_FOR_SAVE, getTodayDateTime(), false)
    }

    fun putSavedDayForCheck(dateToSave: String) {
        preferenceService.putString(DATE_FOR_SAVE, dateToSave, false)
    }

    fun putIsnaik(isNaik: String){
        preferenceService.putString(ISNAIK,isNaik,false)
    }
    fun getIsnaik(): String?{
        return preferenceService.getString(ISNAIK, "", false)
    }

    fun removeIsNaik(){
        return preferenceService.removePreference(ISNAIK)
    }

    fun putVersionForCheck(version: Int) {
        preferenceService.putInt(VERSION_FOR_CHECK, version)
    }

    fun getVersionForCheck(): Int {
        return preferenceService.getInt(VERSION_FOR_CHECK, 0)
    }



}