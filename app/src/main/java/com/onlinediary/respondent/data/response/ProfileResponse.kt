package com.onlinediary.respondent.data.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ProfileResponse(
    @SerializedName("alamat")
    val alamat: String?,
    @SerializedName("birthday")
    val birthday: String?,
    @SerializedName("birthplace")
    val birthplace: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("email_verified_at")
    val emailVerifiedAt: String?,
    @SerializedName("foto")
    val foto: String?,
    @SerializedName("fullname")
    val fullname: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("initial")
    val initial: String?,
    @SerializedName("nickname")
    val nickname: String?,
    @SerializedName("pendidikan")
    val pendidikan: String?,
    @SerializedName("phone")
    val phone: String?,
    @SerializedName("region")
    val region: String?,
    @SerializedName("regions_id")
    val regionsId: String?,
    @SerializedName("role")
    val role: String?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("updated_at")
    val updatedAt: String?
): Parcelable, Serializable {
    companion object{
        fun empty(): ProfileResponse{
            return ProfileResponse(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            )
        }
    }
}