package com.onlinediary.respondent.data.response
import com.google.gson.annotations.SerializedName


data class  GeneralPostResponse(
    @SerializedName("message")
    val message: String?,
    @SerializedName("success")
    val success: Boolean?
)