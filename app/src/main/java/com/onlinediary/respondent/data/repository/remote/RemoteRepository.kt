package com.onlinediary.respondent.data.repository.remote

import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.network.RetrofitClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class RemoteRepository{
    companion object{
        const val LIMIT_SYNC = 100

    }

    private val service = RetrofitClient.getClient(
        BaseApiService.BASE_URL
    )?.create(BaseApiService::class.java)

    /**=========================POST=================================**/
    suspend fun emittedLogin(username : String,fcmToken : String, password : String) =
        service?.postLogin(username,fcmToken, password)

    /*suspend fun emittedPostActivity(accessToken: String, photo : MultipartBody.Part?,name : RequestBody?,story : RequestBody?,projectId : Int?,usedProduct : RequestBody?) =
        service?.postActivity(accessToken, photo, name, story, projectId, usedProduct)
*/
    suspend fun emittedPostActivity(accessToken: String, photo : MultipartBody.Part?,name : RequestBody?, projectId : Int?,usedProduct : RequestBody?) =
        service?.postActivity(accessToken, photo, name, projectId, usedProduct)

    suspend fun emittedUpdateResp(accessToken : String, photo : MultipartBody.Part?, name : RequestBody, email : RequestBody, phone : RequestBody, birthDay : RequestBody, gender : RequestBody, marital : RequestBody, password: RequestBody) =
            service?.updateProfileResp(accessToken,photo,name, email, phone, birthDay,gender, marital, password)

    suspend fun emittedPostComment(accessToken: String, comment : String?, idPost: Int?) =
        service?.postComment(accessToken, comment, idPost)

    /**=========================GET=================================**/
    suspend fun emittedGetListProject(accessToken : String) =
        service?.getListProject(accessToken)
    suspend fun emittedGetActivityList(accessToken: String, idProj : Int, dates : String) =
        service?.getActivityList(accessToken, idProj, dates)
    suspend fun emittedGetProfile(accessToken: String) =
        service?.getProfile(accessToken)
    suspend fun emittedGetComment(accessToken: String, idPost: Int) =
        service?.getCommentPost(accessToken,idPost)

    suspend fun getActById(accessToken: String, idProj: Int) =
        service?.getDetailActById(accessToken, idProj)
}