package com.onlinediary.respondent.data.response


import com.onlinediary.respondent.data.repository.local.entity.CommentItems

class CommentResponse : ArrayList<CommentItems>()