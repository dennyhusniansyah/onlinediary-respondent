package com.onlinediary.respondent.data.response


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("expires_at")
    val expiresAt: String?,
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("token")
    val token: String?,
    @SerializedName("token_type")
    val tokenType: String?,
    @SerializedName("message")
    val message : String?
)