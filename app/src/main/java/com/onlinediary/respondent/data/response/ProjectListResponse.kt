package com.onlinediary.respondent.data.response

import android.os.Parcelable
import com.onlinediary.respondent.data.repository.local.entity.ProjectList
import kotlinx.android.parcel.Parcelize

@Parcelize
class ProjectListResponse : ArrayList<ProjectList>(), Parcelable