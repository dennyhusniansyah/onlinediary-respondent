package com.onlinediary.respondent.data.repository

import com.onlinediary.respondent.data.repository.local.LocalRepository
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.repository.remote.RemoteRepository
import com.onlinediary.respondent.data.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody

class MainRepository (
    private val localRepository: LocalRepository,
    private val remoteRepository: RemoteRepository
): DataSource{
    override suspend fun doLogin(email: String,fcmToken: String, password: String): LoginResponse? {
        return remoteRepository.emittedLogin(email,fcmToken, password)
    }

    override suspend fun postActivity(
        accessToken: String,
        photo: MultipartBody.Part?,
        name: RequestBody?,
        //story: RequestBody?,
        projectId: Int?,
        usedProduct: RequestBody?
    ): GeneralPostResponse? {
        //return remoteRepository.emittedPostActivity(accessToken, photo, name, story, projectId, usedProduct)
        return remoteRepository.emittedPostActivity(accessToken, photo, name, projectId, usedProduct)
    }

    override suspend fun updateResp(accessToken: String, photo: MultipartBody.Part?, name: RequestBody, email: RequestBody, phone: RequestBody, birthDay: RequestBody, gender: RequestBody, marital: RequestBody, password: RequestBody): GeneralPostResponse? {
        return remoteRepository.emittedUpdateResp(accessToken, photo, name, email, phone, birthDay, gender, marital, password)
    }

    override suspend fun postComment(accessToken: String, comment: String?, idPost: Int?): GeneralPostResponse? {
        return  remoteRepository.emittedPostComment(accessToken, comment, idPost)
    }

    override suspend fun getListProject(accessToken: String): ProjectListResponse? {
        return remoteRepository.emittedGetListProject(accessToken)
    }

    override suspend fun getActivityList(accessToken: String, idProj : Int, dates : String): ActivityListResponse? {
        return remoteRepository.emittedGetActivityList(accessToken, idProj, dates)
    }

    override suspend fun getProfile(accessToken: String): Profile? {
        return remoteRepository.emittedGetProfile(accessToken)
    }

    override suspend fun getCommentPost(accessToken: String, idPost: Int): CommentResponse? {
        return remoteRepository.emittedGetComment(accessToken, idPost)
    }

    override suspend fun getActById(accessToken: String, idPost: Int): ActivityList? {
        return remoteRepository.getActById(accessToken, idPost)
    }


    companion object{
        fun getInstance(remoteRepository: RemoteRepository, localRepository: LocalRepository): MainRepository{
            return MainRepository(localRepository,remoteRepository)
        }
    }
}