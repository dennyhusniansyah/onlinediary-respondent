package com.onlinediary.respondent.data.repository.local.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize



@Parcelize
data class ActivityList(
        @SerializedName("comment")
        val comment: Int?,
        @SerializedName("created_at")
        val createdAt: String?,
        @SerializedName("id")
        val id: Int?,
        @SerializedName("name")
        val name: String?,
       /* @SerializedName("story")
        val story: String?,*/
        @SerializedName("photo")
        val photo: String?,
        @SerializedName("project")
        val project: String?,
        @SerializedName("projects_id")
        val projectsId: String?,
        @SerializedName("status")
        val status: String?,
        @SerializedName("user")
        val user: String?,
        @SerializedName("qnaire")
        val qnaire: Int?,
        @SerializedName("user_photo")
        val userPhoto: String?
): Parcelable