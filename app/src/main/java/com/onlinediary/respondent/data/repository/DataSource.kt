package com.onlinediary.respondent.data.repository

import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface DataSource {
    /**remote **/
    //============post====================//
    suspend fun doLogin(email: String,fcmToken: String, password: String): LoginResponse?
    suspend fun postActivity(accessToken: String, photo : MultipartBody.Part?, name : RequestBody?, projectId : Int?, usedProduct : RequestBody?) : GeneralPostResponse?
    //suspend fun postActivity(accessToken: String, photo : MultipartBody.Part?, name : RequestBody?, story : RequestBody?, projectId : Int?, usedProduct : RequestBody?) : GeneralPostResponse?
    suspend fun updateResp(accessToken : String, photo : MultipartBody.Part?, name : RequestBody, email : RequestBody, phone : RequestBody, birthDay : RequestBody, gender : RequestBody, marital : RequestBody, password: RequestBody) : GeneralPostResponse?
    suspend fun postComment(accessToken: String, comment : String?, idPost: Int?) : GeneralPostResponse?


    //============GET====================//
    suspend fun getListProject(accessToken : String): ProjectListResponse?
    suspend fun getActivityList(accessToken: String, idProj : Int, dates : String) : ActivityListResponse?
    suspend fun getProfile(accessToken: String) : Profile?
    suspend fun getCommentPost(accessToken: String, idPost : Int) : CommentResponse?
    suspend fun getActById(accessToken: String, idPost: Int) : ActivityList?

}