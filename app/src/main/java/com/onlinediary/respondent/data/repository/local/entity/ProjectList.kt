package com.onlinediary.respondent.data.repository.local.entity


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProjectList(
    @SerializedName("code")
    val code: String?,
    @SerializedName("days")
    val days: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("finish")
    val finish: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("instruction")
    val instruction: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("product")
    val product: String?,
    @SerializedName("products_id")
    val productsId: String?,
    @SerializedName("quota")
    val quota: String?,
    @SerializedName("realtime")
    val realtime: String?,
    @SerializedName("start")
    val start: String?,
    @SerializedName("task")
    val task: String?,
    @SerializedName("timing")
    val timing: List<Timing>?,
    @SerializedName("qnaire")
    val qnaire: Int?
):Parcelable