package com.onlinediary.respondent.view.main.createpost

import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.webkit.ValueCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.DateTimeHelper
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.onlinediary.respondent.utils.extension.isVisible
import com.onlinediary.respondent.utils.gone
import com.onlinediary.respondent.utils.visible
import com.onlinediary.respondent.view.WebViewActivity
import com.onlinediary.respondent.view.obtainViewModel
import kotlinx.android.synthetic.main.activity_detail_post.*
import org.jetbrains.anko.startActivity
import org.joda.time.DateTime

class DetailPostActivity : AppCompatActivity() {

    private val createPostViewModel : CreatePostViewModel by lazy {
        obtainViewModel()
    }

    private var mCM: String? = null
    private var mUM: ValueCallback<Uri>? = null
    private var mUMA: ValueCallback<Array<Uri>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_post)
        setSupportActionBar(toolbar_detailPost)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val idPost = intent.getStringExtra(ID_POST).toInt()

        //fragment comment
        supportFragmentManager.beginTransaction()
                .replace(R.id.frame_comment, CommentFragment.newInstance(idPost))
                .commitAllowingStateLoss()

        btn_gotoKuesioner_fromDetail.setOnClickListener {
            this.startActivity<WebViewActivity>(WebViewActivity.ID_ACTIVITY_FROM_NOTIF to idPost.toString())
        }


        createPostViewModel.getActById(idPost)

        observeData()

        if (btn_reload_DetailAct.isVisible()){
            btn_reload_DetailAct.setOnClickListener {
                createPostViewModel.getActById(idPost)
            }
        }

    }

    private fun observeData() {
        createPostViewModel.progress.observe(this, androidx.lifecycle.Observer {
            when(it){
                ProgressLoading.LOADING->{
                    progress_detailPost.visible()
                    root_detailAct.gone()
                    layout_noInternet_DetailAct.gone()
                }
                ProgressLoading.DONE->{
                    progress_detailPost.gone()
                    root_detailAct.visible()
                    layout_noInternet_DetailAct.gone()

                }
                ProgressLoading.ERROR->{
                    progress_detailPost.gone()
                    root_detailAct.gone()
                    layout_noInternet_DetailAct.visible()
                }
            }

        })


        createPostViewModel.post.observe(this, androidx.lifecycle.Observer {
            if (it!=null){

                populateData(it)

                RxBus.publish(RxEvent.sendActivityDetail(it))


//                val adapter = TabAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, it)
//                viewPagerDetail.adapter = adapter
//                tablayoutDetail.setupWithViewPager(viewPagerDetail)

            }
        })
    }

    private fun populateData(detailPostData: ActivityList?) {
   //     tv_nameDetailPost.text = detailPostData?.user
        tv_dateDetailPost.text = DateTimeHelper.getDatewithHour(DateTime(detailPostData?.createdAt))
        tv_captionDetailPost.text = detailPostData?.name

  //      tv_statusPostDetail.text = detailPostData?.status

        when(detailPostData?.status){
            "pending"->{
                iv_statusPostDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_warning))

            }
            "approved"->{
                iv_statusPostDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checklist))

            }
            "rejected"->{
                iv_statusPostDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_rejected))

            }
        }




//        Glide.with(this)
//            .load(BaseApiService.BASE_IMAGE_PROFILE_URL + detailPostData?.userPhoto)
//            .into(iv_imageProfileDetailPost)
//
//
//        val newHeight = this.resources.displayMetrics.widthPixels
//
//        iv_imageDetailPost.layoutParams.height = newHeight
//
//
//        Glide.with(this)
//            .load(BaseApiService.BASE_IMAGE_ACTIVITY_URL + detailPostData?.photo)
//            .into(iv_imageDetailPost)

        val url = BaseApiService.BASE_URL + "qna/${detailPostData?.id}"

//
//        webView_detail_act.isVerticalScrollBarEnabled = true
//        webView_detail_act.isHorizontalScrollBarEnabled = true
//        webView_detail_act.settings.javaScriptEnabled = true
//        webView_detail_act.settings.domStorageEnabled = true
//        webView_detail_act.settings.allowFileAccess = true
//        webView_detail_act.settings.allowContentAccess = true
//        webView_detail_act.settings.setSupportZoom(false)
//
//        val headerMap = HashMap<String, String>()
//        if (createPostViewModel.token != null) {
//            headerMap["Authorization"] = createPostViewModel.token!!
//        }
//
//        webView_detail_act.webChromeClient = object : WebChromeClient() {
//            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
//                if (mUMA != null) {
//                    mUMA!!.onReceiveValue(null)
//                }
//                mUMA = filePathCallback
//                var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//                if (takePictureIntent!!.resolveActivity(this@DetailPostActivity.packageManager) != null) {
//                    var photoFile: File? = null
//                    try {
//                        photoFile = createImageFile()
//                        takePictureIntent.putExtra("PhotoPath", mCM)
//                    } catch (ex: IOException) {
//                        Log.e("Webview", "Image file creation failed", ex)
//                    }
//                    if (photoFile != null) {
//                        mCM = "file:" + photoFile.absolutePath
//                        takePictureIntent.putExtra(
//                                MediaStore.EXTRA_OUTPUT,
//                                Uri.fromFile(photoFile)
//                        )
//                    } else {
//                        takePictureIntent = null
//                    }
//                }
//                val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
//                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
//                contentSelectionIntent.type = "*/*"
//                val intentArray: Array<Intent>
//                intentArray = takePictureIntent?.let { arrayOf(it) } ?: arrayOf<Intent>()
//                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
//                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
//                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
//                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
//                startActivityForResult(chooserIntent, FCR)
//                return true
//            }
//
//
//        }
//        webView_detail_act.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
//                //url will be the url that you click in your webview.
//                //you can open it with your own webview or do
//                //whatever you want
//
//                //Here is the example that you open it your own webview.
//                view.loadUrl(url)
//                return true
//            }
//        }
//
//        webView_detail_act.loadUrl(url, headerMap)
//      //  webView_detail_act.postUrl(url, ByteArray(0))
//



    }

//    // Create an image file
//    @Throws(IOException::class)
//    private fun createImageFile(): File? {
//        @SuppressLint("SimpleDateFormat") val timeStamp: String =
//                SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
//        val imageFileName = "img_" + timeStamp + "_"
//        val storageDir: File =
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
//        return File.createTempFile(imageFileName, ".jpg", storageDir)
//    }
//    fun openFileChooser(uploadMsg: ValueCallback<Uri?>?) {
//        this.openFileChooser(uploadMsg, "*/*")
//    }
//
//    fun openFileChooser(
//            uploadMsg: ValueCallback<Uri?>?,
//            acceptType: String?
//    ) {
//        this.openFileChooser(uploadMsg, acceptType, null)
//    }
//
//    fun openFileChooser(
//            uploadMsg: ValueCallback<Uri?>?,
//            acceptType: String?,
//            capture: String?
//    ) {
//        val i = Intent(Intent.ACTION_GET_CONTENT)
//        i.addCategory(Intent.CATEGORY_OPENABLE)
//        i.type = "*/*"
//        this.startActivityForResult(Intent.createChooser(i, "File Browser"), FCR)
//    }


//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
//        super.onActivityResult(requestCode, resultCode, intent)
//        if (Build.VERSION.SDK_INT >= 21) {
//            var results: Array<Uri>? = null
//            //Check if response is positive
//            if (resultCode == Activity.RESULT_OK) {
//                if (requestCode == FCR) {
//                    if (null == mUMA) {
//                        return
//                    }
//                    if (intent == null) { //Capture Photo if no image available
//                        if (mCM != null) {
//                            results = arrayOf(Uri.parse(mCM))
//                        }
//                    } else {
//                        val dataString = intent.dataString
//                        if (dataString != null) {
//                            results = arrayOf(Uri.parse(dataString))
//                        }
//                    }
//                }
//            }
//            mUMA!!.onReceiveValue(results)
//            mUMA = null
//        } else {
//            if (requestCode == FCR) {
//                if (null == mUM) return
//                val result =
//                        if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
//                mUM!!.onReceiveValue(result)
//                mUM = null
//            }
//        }
//    }




    companion object{
        private const val FCR = 1
        const val ID_POST = "ID Post"

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun obtainViewModel(): CreatePostViewModel = obtainViewModel(CreatePostViewModel::class.java)


}