package com.onlinediary.respondent.view.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.onlinediary.respondent.R
import com.onlinediary.respondent.utils.extension.*
import com.onlinediary.respondent.view.main.dashboard.DashboardActivity
import com.onlinediary.respondent.view.obtainViewModel
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.layout_login.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity() {
    private val loginViewModel : LoginViewModel by lazy {
        obtainViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_login)
        bindProgressButton(btn_login)
        makeStatusBarTransparent()
        iv_logo.setMarginTop(100)


        KeyboardVisibilityEvent.setEventListener(this, object : KeyboardVisibilityEventListener{
            override fun onVisibilityChanged(isOpen: Boolean) {
                if (!isOpen){
                    iv_logo.makeVisible()
                }else{
                    iv_logo.makeGone()

                }
            }

        })

        btn_login.setOnClickListener {
            val username = et_username.text.toString()
            val password = et_password.text.toString()


            if (username==""|| password==""){
                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("FCM_WARNING", "Fetching FCM registration token failed", task.exception)
                        return@OnCompleteListener
                    }
                    // Get new FCM registration token
                    val token = task.result

                    // Log and toast
                    val msg = getString(R.string.msg_token_fmt, token)
                    Log.d("TOKEN_FCM", token)

                    toast("Isian tidak boleh kosong || $msg")

                })
            }else{
                btn_login.showProgress {
                    progressColor = Color.WHITE
                }
                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("FCM_WARNING", "Fetching FCM registration token failed", task.exception)
                        return@OnCompleteListener
                    }
                    // Get new FCM registration token
                    val token = task.result

                    // Log
                    Log.d("TOKEN_FCM", token)

                    if (token != null) {
                        loginViewModel.doLogin(this, username,token, password)
                        loginViewModel.resultLogin.observe(this, Observer {
                            Log.d("GETRESPONSE", "$it")
                            if (it?.success == true){
                                showToast("Login Berhasil")
                                loginViewModel.storeLoginCredentials(it)

                                startActivity(Intent(this, DashboardActivity::class.java))
                                finish()

                            }

                        })
                    }



                })


            }
        }


        loginViewModel.requestResult.observe(this, Observer {
            if (it!=null){
                btn_login.hideProgress("Login")
                showToast(it)
            }
        })

    }



    private fun obtainViewModel(): LoginViewModel = obtainViewModel(LoginViewModel::class.java)


}
