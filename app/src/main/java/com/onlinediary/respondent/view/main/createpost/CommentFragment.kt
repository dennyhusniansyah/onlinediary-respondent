package com.onlinediary.respondent.view.main.createpost

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.onlinediary.respondent.R
import com.onlinediary.respondent.adapter.CommentItem
import com.onlinediary.respondent.data.repository.local.entity.CommentItems
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.onlinediary.respondent.utils.extension.clearText
import com.onlinediary.respondent.utils.gone
import com.onlinediary.respondent.utils.invisible
import com.onlinediary.respondent.utils.visible
import com.onlinediary.respondent.view.obtainViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_comment.*
import org.jetbrains.anko.support.v4.toast


class CommentFragment : Fragment() {

    companion object{
        const val ACT_DETAIL = "ACT_DETAIL"

        @JvmStatic
        fun newInstance(idAct: Int): CommentFragment {
            val bundle = Bundle().apply {
                putInt(ACT_DETAIL, idAct)
            }

            return CommentFragment().apply {
                arguments = bundle
            }
        }


    }

    private val createPostViewModel : CreatePostViewModel by lazy {
        obtainViewModel()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comment, container, false)
    }

    var idPost : Int? = 0


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val actdetail = arguments?.getInt(QuestionerFragment.ACT_DETAIL)

        idPost = actdetail


        idPost?.let { createPostViewModel.getComment(it) }

        observeData()
        configLayout()


        onPostComment()




    }

    private fun onPostComment() {

        btn_postComment.setOnClickListener {
            val comment = et_textComment.text.toString()
            if (comment!=""){
                createPostViewModel.postComment(comment, idPost)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun configLayout() {

        et_textComment.setOnTouchListener(OnTouchListener { v, event ->
            if (et_textComment.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@OnTouchListener true
                    }
                }
            }
            false
        })
    }

    private fun observeData() {
        createPostViewModel.comment.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()){
                rv_itemComment.invisible()
                layout_noData_comment.visible()

            }else{
                rv_itemComment.visible()
                layout_noData_comment.gone()
                initReyclerView(it.toRecyclerListItem())
            }
        })


        createPostViewModel.progressPostComment.observe(viewLifecycleOwner, Observer {
            when(it){
                ProgressLoading.LOADING->{
                    btn_postComment.gone()
                    progress_postComment.visible()
                }
                ProgressLoading.DONE->{
                    et_textComment.clearText()
                    btn_postComment.visible()
                    RxBus.publish(RxEvent.isRefreshing(true))
                    progress_postComment.gone()
                    idPost?.let { it1 -> createPostViewModel.getComment(it1) }
                }
                ProgressLoading.ERROR->{
                    btn_postComment.gone()
                    progress_postComment.visible()
                }

            }
        })


        createPostViewModel.error.observe(viewLifecycleOwner, Observer {
            if (it!=""){
                toast(it)
            }
        })


    }

    private fun initReyclerView(items : List<CommentItem>){
        val  groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(items)
        }
        groupAdapter.notifyDataSetChanged()

        rv_itemComment.apply {
            adapter = groupAdapter
            scrollToPosition(items.size - 1)
        }
    }

    private fun List<CommentItems>.toRecyclerListItem() : List<CommentItem>{
        return this.map { comment->
            CommentItem(comment)
        }
    }



    private fun obtainViewModel(): CreatePostViewModel = obtainViewModel(CreatePostViewModel::class.java)


}