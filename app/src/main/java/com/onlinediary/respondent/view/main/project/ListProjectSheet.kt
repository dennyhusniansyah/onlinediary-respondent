package com.onlinediary.respondent.view.main.project

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.onlinediary.respondent.R
import com.onlinediary.respondent.adapter.ProjectItem
import com.onlinediary.respondent.data.repository.local.entity.ProjectList
import com.onlinediary.respondent.data.response.ProjectListResponse
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_list_project_sheet.*


class ListProjectSheet : BottomSheetDialogFragment(), ProjectItem.ProjectListListener {

    companion object{
        const val PROJECT_LIST = "PROJECT_LIST"

        @JvmStatic
        fun newInstance(projectListResponse: ProjectListResponse): ListProjectSheet{
            val bundle = Bundle().apply {
                putParcelable(PROJECT_LIST, projectListResponse)
            }

            return ListProjectSheet().apply {
                arguments = bundle
            }
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_project_sheet, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val projectList = arguments?.getParcelable<ProjectListResponse>(PROJECT_LIST)

        projectList?.toRecyclerListItem()?.let { initRecylerView(it) }


    }


    private fun initRecylerView(items : List<ProjectItem>){
        val  groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(items)
        }
        groupAdapter.notifyDataSetChanged()

        rv_listProject.apply {
            adapter = groupAdapter
        }
    }

    private fun List<ProjectList>.toRecyclerListItem() : List<ProjectItem>{
        return this.map {project ->
            ProjectItem(project, this@ListProjectSheet)
        }
    }

    override fun onProjectClick(project: ProjectList) {
        RxBus.publish(RxEvent.sendProjectItem(project))
        RxBus.publish(RxEvent.isRefreshing(true))
        dismiss()
    }


}