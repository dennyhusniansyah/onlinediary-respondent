package com.onlinediary.respondent.view.main.createpost

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.onlinediary.respondent.data.repository.MainRepository
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.data.response.CommentResponse
import com.onlinediary.respondent.utils.ErrorHelper
import com.onlinediary.respondent.utils.EventHelper
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.extension.toAuthorization
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

class CreatePostViewModel (application: Application, private val mainRepository: MainRepository, preferenceService: PreferenceService): AndroidViewModel(application) {

    private val localServiceManager = LocalServiceManager(preferenceService)
    val token = localServiceManager.getAccessTokenCredentials()?.toAuthorization()

    private val mProgress = MutableLiveData<ProgressLoading>()
    private val mProgressPostComment = MutableLiveData<ProgressLoading>()
    private val mError = MutableLiveData<String>()

    private val mComment = MutableLiveData<CommentResponse?>()
    private val mPost = MutableLiveData<ActivityList?>()

    val progress : LiveData<ProgressLoading>
        get() = mProgress
    val progressPostComment : LiveData<ProgressLoading>
        get() = mProgressPostComment

    val error : MutableLiveData<String>
        get() = mError

    val comment : LiveData<CommentResponse?>
        get() = mComment
    val post : LiveData<ActivityList?>
        get() = mPost

    private val statusMessage = MutableLiveData<EventHelper<String>>()

    val message : LiveData<EventHelper<String>>
        get() = statusMessage

    fun createPost(
        photo : MultipartBody.Part?,
        name : RequestBody?,
        //story : RequestBody?,
        projectId : Int?,
        usedProduct : RequestBody?
    ){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {

                //delay(5000) //testing delay

                //token?.let { mainRepository.postActivity(it, photo, name, story, projectId, usedProduct) }
                token?.let { mainRepository.postActivity(it, photo, name, projectId, usedProduct) }
                mProgress.value = ProgressLoading.DONE


            }catch (e: Throwable){
                e.printStackTrace()
                mProgress.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)
            }
        }
    }


    fun getActById(idPost : Int){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val post = token?.let { mainRepository.getActById(it, idPost) }
                mPost.value = post
                mProgress.value = ProgressLoading.DONE

            }catch (e : Throwable){
                e.printStackTrace()
                mProgress.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)

            }
        }
    }


    //===========comment=========//

    fun getComment(idPost : Int){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val comment = token?.let { mainRepository.getCommentPost(it, idPost) }
                mComment.value = comment
                mProgress.value = ProgressLoading.DONE

            }catch (e : Throwable){
                e.printStackTrace()
                mProgress.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)

            }
        }

    }

    fun postComment(comment : String?, idPost: Int?){
        mProgressPostComment.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                token?.let { mainRepository.postComment(it, comment, idPost) }
                mProgressPostComment.value = ProgressLoading.DONE

            }catch (e: Throwable){
                e.printStackTrace()

                mProgressPostComment.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)

            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }


}