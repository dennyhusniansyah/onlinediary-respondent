package com.onlinediary.respondent.view.main.profile

import android.app.Activity
import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.DateTimeHelper
import com.onlinediary.respondent.view.obtainViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.github.razir.progressbutton.bindProgressButton
import kotlinx.android.synthetic.main.activity_edit_profile.*
import androidx.lifecycle.Observer
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class EditProfileActivity : AppCompatActivity() {
    
    private val profileViewModel : ProfileViewModel by lazy { 
        obtainViewModel()
    }

    private val listGender = ArrayList<String>()
    private val listMarital = ArrayList<String>()
    private var filePhoto : File? = null
    private var filePath = ""
    

    private lateinit var detailProfile : Profile
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        setSupportActionBar(toolbar_editResponder)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        bindProgressButton(btn_editProfilePost)

        detailProfile = intent.getParcelableExtra(ProfileActivity.DETAIL_PROFILE)

        if (detailProfile!=null){
            populateData(detailProfile)
        }


        preparePost()

        observeData()
        
        
    }

    private fun observeData() {
        profileViewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING->{
                    btn_editProfilePost.showProgress {
                        progressColor = Color.WHITE
                    }
                }
                ProgressLoading.DONE->{
                    btn_editProfilePost.hideProgress("Tambahkan")
                    toast("Berhasil memperbarui profile!")
                    RxBus.publish(RxEvent.isRefreshing(true))
                    finish()
                }
                ProgressLoading.ERROR->{
                    btn_editProfilePost.hideProgress("Tambahkan")
                    btn_editProfilePost.icon = ContextCompat.getDrawable(this,R.drawable.ic_add)
                }
            }
        })

        profileViewModel.error.observe(this, Observer {
            toast(it)
        })
    }

    private fun preparePost() {
        btn_editProfilePost.setOnClickListener {
            val name = et_nameResponder.getValue()
            val email = et_emailResponder.getValue()
            val phone = et_phoneResponder.getValue()
            val birthDay = et_birthResponder.getValue()
            val gender = if (spinner_genderResponder.selectedItem as String == "Male") "M" else "F"
            val marital = spinner_maritalResponder.selectedItem as String
            val password = et_passwordResponder.text.toString()


            if (name==""||email==""||phone==""||birthDay==""||gender==""||marital=="") {
                toast("isian tidak boleh kosong!")
            }else {
                val alert = AlertDialog.Builder(this)
                alert.setCancelable(true)
                alert.setTitle("Data sudah benar?")
                alert.setMessage("Anda yakin data sudah benar?")
                alert.setPositiveButton("YA") { dialogInterface, i ->
                    dialogInterface.dismiss()

                    /***POST Responder***/

                    val namePart = RequestBody.create(MediaType.parse("text/plain"), name)
                    val emailPart = RequestBody.create(MediaType.parse("text/plain"), email)
                    val phonePart = RequestBody.create(MediaType.parse("text/plain"), phone)
                    val birthDayPart = RequestBody.create(MediaType.parse("text/plain"), birthDay)
                    val genderPart = RequestBody.create(MediaType.parse("text/plain"), gender)
                    val maritalPart = RequestBody.create(MediaType.parse("text/plain"), marital)
                    val passwordPart = RequestBody.create(MediaType.parse("text/plain"), password)


                    if (filePhoto!=null){
                        val body = RequestBody.create(MediaType.parse("multipart/form-file"), filePhoto!!)
                        val photo = MultipartBody.Part.createFormData("photo", filePhoto?.name, body)

                        profileViewModel.postEditProfile(photo, namePart, emailPart,phonePart, birthDayPart,genderPart,maritalPart,passwordPart)

                    }else{
                        profileViewModel.postEditProfile(null, namePart, emailPart,phonePart, birthDayPart,genderPart,maritalPart,passwordPart)

                    }




                }

                alert.setNegativeButton("Tidak") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }

                val dialog: AlertDialog = alert.create()
                dialog.show()
            }

        }
    }

    private fun populateData(detailProfile: Profile) {
        //Setting spinner

        listGender.add("Male")
        listGender.add("Female")

        listMarital.add("Yes")
        listMarital.add("No")

        var marital  = ""
        var gender = ""

        gender = if (detailProfile.gender =="M"){
            "Male"
        }else{
            "Female"
        }

        marital = if (detailProfile.marital =="yes"){
            "Yes"
        }else{
            "No"
        }

        val indexGender = listGender.indexOf(gender)
        val indexMarital = listMarital.indexOf(marital)


        val adapterGender  = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listGender)
        val adapterMarital  = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listMarital)


        spinner_genderResponder.adapter = adapterGender
        spinner_maritalResponder.adapter = adapterMarital

        spinner_maritalResponder.setSelection(indexMarital)
        spinner_genderResponder.setSelection(indexGender)


        //populate edittext
        detailProfile.name?.let { et_nameResponder.setValue(it) }
        detailProfile.email?.let { et_emailResponder.setValue(it) }
        detailProfile.birthday?.let { et_birthResponder.setValue(it) }
        detailProfile.phone?.let { et_phoneResponder.setValue(it) }


        if (detailProfile.photo!=null){
            Glide.with(this)
                    .load(BaseApiService.BASE_IMAGE_PROFILE_URL + detailProfile.photo)
                    .into(iv_profile_edit)
        }


        et_birthResponder.setOnClickListener {
            generateCalendar()
        }


        iv_profile_edit.setOnClickListener {
            ImagePicker.with(this)
                    .crop(1f, 1f)
                    .start{resultCode, data ->

                        when (resultCode) {
                            Activity.RESULT_OK -> {
                                //Image Uri will not be null for RESULT_OK
                                val fileUri = data?.data
                                iv_profile_edit.setImageURI(fileUri)

                                //You can get File object from intent
                                filePhoto = ImagePicker.getFile(data)!!

                                //You can also get File Path from intent
                                filePath = ImagePicker.getFilePath(data)!!
                            }
                            ImagePicker.RESULT_ERROR -> {
                                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                Log.d("CANCELLED", "Task Cancelled")
                            }
                        }

                    }
        }


    }

    private fun generateCalendar() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DAY_OF_MONTH,dayOfMonth)
            val date = DateTimeHelper.historyDateFormatFLIP.print(c.timeInMillis)

            et_birthResponder.setValue(date)

        }, year, month, day)

        dpd.datePicker.maxDate = Date().time
        dpd.show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun obtainViewModel(): ProfileViewModel = obtainViewModel(ProfileViewModel::class.java)


}