package com.onlinediary.respondent.view.main.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.onlinediary.respondent.data.repository.MainRepository
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.utils.ErrorHelper
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.extension.toAuthorization
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ProfileViewModel (application: Application, private val mainRepository: MainRepository, preferenceService: PreferenceService): AndroidViewModel(application)
{
    private val localServiceManager = LocalServiceManager(preferenceService)
    private val token = localServiceManager.getAccessTokenCredentials()?.toAuthorization()

    private val mProfile = MutableLiveData<Profile?>()
    private val mProgress = MutableLiveData<ProgressLoading>()
    private val mError = MutableLiveData<String>()


    val profileItem: LiveData<Profile?>
        get() = mProfile

    val progress: LiveData<ProgressLoading>
        get() = mProgress
    val error: MutableLiveData<String>
        get() = mError



    fun getProfile(){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = token?.let { mainRepository.getProfile(it) }

                mProfile.value = response

                mProgress.value = ProgressLoading.DONE

            }catch (e:Throwable){
                e.printStackTrace()
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)
                mProgress.value = ProgressLoading.ERROR

            }
        }
    }


    fun postEditProfile(
            photo: MultipartBody.Part?,
            name: RequestBody,
            email: RequestBody,
            phone: RequestBody,
            birthDay: RequestBody,
            gender: RequestBody,
            marital: RequestBody,
            password: RequestBody
    ) {
        viewModelScope.launch {
            mProgress.value = ProgressLoading.LOADING
            try {
                val response = token?.let {
                    mainRepository.updateResp(
                            it,
                            photo,
                            name,
                            email,
                            phone,
                            birthDay,
                            gender,
                            marital,
                            password
                    )
                }

                if (response?.success == false) {
                    mError.value = ErrorHelper.createCredentialFailedMessage(
                            getApplication(),
                            response.message
                    )
                    mProgress.value = ProgressLoading.ERROR
                } else {
                    mProgress.value = ProgressLoading.DONE
                }

            } catch (e: Throwable) {
                e.printStackTrace()
                mProgress.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)
            }
        }
    }


    fun doLogout(){
        localServiceManager.removeAccessTokenCredentials()
        localServiceManager.removeProfileCredentials()
        localServiceManager.removeSession()
    }



    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }




}