package com.onlinediary.respondent.view.main.createpost

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.github.dhaval2404.imagepicker.ImagePicker
import com.onlinediary.respondent.R
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.onlinediary.respondent.view.obtainViewModel
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.onlinediary.respondent.utils.gone
import com.onlinediary.respondent.utils.visible
import kotlinx.android.synthetic.main.activity_create_post.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.File


class CreatePostActivity : AppCompatActivity() {

    private val createPostViewModel : CreatePostViewModel by lazy {
        obtainViewModel()
    }

    private var filePhoto : File? = null
    private var filePath = ""
    private var idProject = 0

    companion object{
        const val ID_PROJECT = "ID_PROJECT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)
        setSupportActionBar(toolbar_addPost)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        bindProgressButton(btn_createPost)

        idProject = intent.getIntExtra(ID_PROJECT, 0)

        et_textPost.requestFocus()

        observeData()

        populateAction(idProject)

    }

    private fun observeData() {
        val dialog = AlertDialog.Builder(this).create()
        val dialogView = layoutInflater.inflate(R.layout.layout_dialog_quest, null)
        dialog.setView(dialogView)
        dialog.setCancelable(false)
        val btn_dismiss = dialogView.findViewById<LinearLayout>(R.id.btn_dismiss_dialogPost)

       createPostViewModel.progress.observe(this, Observer {

           when(it){
              ProgressLoading.LOADING->{
                  btn_createPost.showProgress {
                      progressColor = Color.WHITE
                  }
              }
              ProgressLoading.DONE->{
                  btn_createPost.hideProgress("Post")
                  dialog.show()
                  btn_dismiss.setOnClickListener {
                      dialog.dismiss()
                      RxBus.publish(RxEvent.isRefreshing(true))
                      dialog.dismiss()
                      finish()
                  }
                  Handler().postDelayed({
                      RxBus.publish(RxEvent.isRefreshing(true))
                      dialog.dismiss()
                      finish()
                  }, 7000)


              }
              ProgressLoading.ERROR->{
                  btn_createPost.hideProgress("Post")
              }
          }
        })

        createPostViewModel.error.observe(this, Observer {
            toast(it)
        })
    }

    private fun populateAction(idProject : Int) {
        btn_addImagePost.setOnClickListener {
            ImagePicker.with(this)
                    .cameraOnly()
                    .cropSquare()
                    .compress(512)
                    .maxResultSize(1080, 1080)
                    .start{resultCode, data ->
                        when (resultCode) {
                            Activity.RESULT_OK -> {
                                //Image Uri will not be null for RESULT_OK
                                val fileUri = data?.data
                                iv_imagePost.setImageURI(fileUri)
                                layout_imagePost.visible()
                                btn_addImagePost.gone()

                                //You can get File object from intent
                                filePhoto = ImagePicker.getFile(data)!!

                                //You can also get File Path from intent
                                filePath = ImagePicker.getFilePath(data)!!

                            }
                            ImagePicker.RESULT_ERROR -> {
                                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                Log.d("CANCELLED", "Task Cancelled")
                            }
                        }
                    }
        }

        btn_removeImagePost.setOnClickListener {
            iv_imagePost.setImageResource(0)
            filePhoto = null
            filePath = ""
            layout_imagePost.gone()
            btn_addImagePost.visible()
        }

        btn_createPost.setOnClickListener {
            val caption = et_textPost.text.toString()

            //cek field postingan

            //if (caption==""){
            if (filePhoto==null||filePath==""||caption==""){
                //tampilkan toast jika postingan kosong
                    if (filePhoto==null && filePath=="" && caption=="") {
                        toast("Mohon isi cerita aktifitas kamu dan ambil foto selfie!")
                    }else if (filePhoto==null && filePath=="") {
                        toast("Mohon ambil foto selfie!")
                    } else if (caption=="") {
                        toast("Mohon isi cerita aktifitas kamu!")
                    }

            }else{

                //isian
                val body = RequestBody.create(MediaType.parse("multipart/form-file"), filePhoto)
                val photo = MultipartBody.Part.createFormData("photo", filePath, body)
                val namePart = RequestBody.create(MediaType.parse("text/plain"),caption)
                //val story = RequestBody.create(MediaType.parse("text/plain"),caption)
                val projectId = idProject


                //if use product, value == YES
                val usedProductPart = RequestBody.create(MediaType.parse("text/plain"),"")
                createPostViewModel.createPost(photo,namePart,projectId,usedProductPart)

                //create alert dialog for using product
//                val message = SpannableStringBuilder()
//                    .append("Apakah pada aktifitas kali ini berinteraksi dengan ")
//                    .bold { append(project.product) }
//                    .append("?")
//
//                val alert = AlertDialog.Builder(this)
//                    .setCancelable(true)
//                    .setTitle("Reminder!")
//                    .setMessage(message)
//                    .setPositiveButton("Ya") { dialogInterface, i ->
//                        dialogInterface.dismiss()
//
//                        //if use product, value == YES
//                        val usedProductPart = RequestBody.create(MediaType.parse("text/plain"),"yes")
//                        createPostViewModel.createPost(photo,namePart,projectId,usedProductPart)
//
//
//                    }
//                    .setNegativeButton("Tidak"){dialogInterface, i ->
//                        dialogInterface.dismiss()
//
//                        //if !use product, value == NO
//                        val usedProductPart = RequestBody.create(MediaType.parse("text/plain"),"no")
//                        createPostViewModel.createPost(photo,namePart,projectId,usedProductPart)
//
//                    }
//
//                val dialog : AlertDialog = alert.create()
//                dialog.show()

            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun obtainViewModel(): CreatePostViewModel = obtainViewModel(CreatePostViewModel::class.java)

}