package com.onlinediary.respondent.view.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.onlinediary.respondent.data.repository.MainRepository
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.data.response.LoginResponse
import com.onlinediary.respondent.utils.ErrorHelper
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class LoginViewModel(preferenceService: PreferenceService, private val mainRepository: MainRepository) : ViewModel(){
    private val mLogin = MutableLiveData<LoginResponse?>()
    private val mresult = MutableLiveData<String>()
    private val localServiceManager = LocalServiceManager(preferenceService)


    val resultLogin : LiveData<LoginResponse?>
        get() = mLogin
    val requestResult: LiveData<String>
        get() = mresult


    /**Login function**/
    fun doLogin(context: Context,username : String,fcmToken : String, password : String){
        viewModelScope.launch {
            try {
                val response = mainRepository.doLogin(username,fcmToken,password)
                Log.d("RESPONSESERVER", "$response")

                mLogin.value = response

            }catch (e : Throwable){
                mresult.value = ErrorHelper.createErrorMessage(context,e)
                e.printStackTrace()

            }
        }
    }


    /**Store storeLogin Credentials**/
    fun storeLoginCredentials(userDetail: LoginResponse?) {
        localServiceManager.saveAccessTokenCredentials(userDetail?.token ?: "")
        localServiceManager.startSession()
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }


}