package com.onlinediary.respondent.view

import android.Manifest
import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.utils.extension.makeStatusBarTransparent
import com.onlinediary.respondent.view.main.dashboard.DashboardActivity
import com.onlinediary.respondent.view.login.LoginActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import net.danlew.android.joda.JodaTimeAndroid

class SplashActivity : AppCompatActivity(), MultiplePermissionsListener {

    private val preferenceService = PreferenceService(this)
    private val localServiceManager = LocalServiceManager(preferenceService)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        JodaTimeAndroid.init(this)
        makeStatusBarTransparent()
        setupPermission()


    }

    private fun setupPermission() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN
            )
            .withListener(this)
            .check()
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun showPermissionRationale(token: PermissionToken) {
        AlertDialog.Builder(this).setTitle(R.string.permission_rationale_title)
            .setMessage(R.string.permission_rationale_message)
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
                token.cancelPermissionRequest()
            }
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                token.continuePermissionRequest()
            }
            .setOnDismissListener { token.cancelPermissionRequest() }
            .show()


    }

    override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
        if (p0?.areAllPermissionsGranted()!!) {
            gotoHome()
        }

    }

    private fun gotoHome() {
        Handler().postDelayed({
            if (localServiceManager.isSessionActive()){
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, 2000)
    }


    override fun onPermissionRationaleShouldBeShown(
        p0: MutableList<PermissionRequest>?,
        p1: PermissionToken?
    ) {
        p1?.let { showPermissionRationale(it) }
    }
}