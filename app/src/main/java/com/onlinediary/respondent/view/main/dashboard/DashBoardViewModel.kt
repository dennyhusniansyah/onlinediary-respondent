package com.onlinediary.respondent.view.main.dashboard

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.onlinediary.respondent.data.repository.MainRepository
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.data.response.ActivityListResponse
import com.onlinediary.respondent.data.response.ProjectListResponse
import com.onlinediary.respondent.utils.ErrorHelper
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.extension.toAuthorization
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DashBoardViewModel (application: Application,private val mainRepository: MainRepository, preferenceService: PreferenceService): AndroidViewModel(application){
    private val localServiceManager = LocalServiceManager(preferenceService)
    private val token = localServiceManager.getAccessTokenCredentials()?.toAuthorization()
    private val mListProject = MutableLiveData<ProjectListResponse?>()
    private val mListActivity = MutableLiveData<ActivityListResponse?>()
    private val mProfile = MutableLiveData<Profile?>()

    private val mProgress = MutableLiveData<ProgressLoading>()
    private val mError = MutableLiveData<String>()

    val listProject : LiveData<ProjectListResponse?>
        get() = mListProject
    val listActivity : LiveData<ActivityListResponse?>
        get() = mListActivity
    val profile : LiveData<Profile?>
        get() = mProfile

    val progress : LiveData<ProgressLoading>
        get() = mProgress
    val error : MutableLiveData<String>
        get() = mError

    fun getDatadashBoard(){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val project = token?.let { mainRepository.getListProject(it) }
                val profile = token?.let { mainRepository.getProfile(it) }
                //assign to livedata
                mListProject.value = project
                mProfile.value = profile
                mProgress.value = ProgressLoading.DONE

            } catch (e: Throwable) {
                e.printStackTrace()
                mProgress.value = ProgressLoading.ERROR
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)
            }
        }
    }

    fun getPostActiviyList(idProj : Int, dates : String){
        viewModelScope.launch {
            try {
                val postActivity = token?.let { mainRepository.getActivityList(it, idProj, dates) }
                mListActivity.value = postActivity

            }catch (e: Throwable){
                e.printStackTrace()
                mError.value = ErrorHelper.createErrorMessage(getApplication(), e)

            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}