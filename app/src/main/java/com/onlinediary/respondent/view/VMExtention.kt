package com.onlinediary.respondent.view

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

fun <T : ViewModel> AppCompatActivity.obtainViewModel(
viewModelClass: Class<T>,
viewModelFactory: ViewModelProvider.Factory? = null
) : T {
    return if (viewModelFactory == null) {
        ViewModelProvider(this, ViewModelFactory.getInstance(application)).get(viewModelClass)
    } else {
        ViewModelProvider(this, viewModelFactory).get(viewModelClass)
    }
}


fun <T : ViewModel> Fragment.obtainViewModel(
    viewModelClass: Class<T>,
    viewModelFactory: ViewModelProvider.Factory? = null
) : T {
    return if (viewModelFactory == null) {
        ViewModelProvider(this, ViewModelFactory.getInstance(activity!!.application)).get(viewModelClass)
    } else {
        ViewModelProvider(this, viewModelFactory).get(viewModelClass)
    }
}
