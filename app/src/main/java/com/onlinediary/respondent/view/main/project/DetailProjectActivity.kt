package com.onlinediary.respondent.view.main.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ProjectList
import com.onlinediary.respondent.utils.DateTimeHelper
import com.onlinediary.respondent.view.main.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_detail_project.*

class DetailProjectActivity : AppCompatActivity() {

    private lateinit var project : ProjectList


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_project)
        setSupportActionBar(toolbar_detailProj)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


         project = intent.getParcelableExtra(DashboardActivity.DETAIL_PROJECT)
        Log.d("PROJECT_DETAIL", "$project")

        populateData(project)


    }

    private fun populateData(project: ProjectList?) {
        toolbar_detailProj.title = project?.name
        tv_projectDetail_Task.text = project?.task
        tv_projectDetail_desc.text = project?.description
        tv_projectDetail_instruksi.text = project?.instruction
//        tv_projectDetail_Deadline.text = "${project?.start?.let { DateTimeHelper.getShortDate(DateTime(it) ) }} - ${project?.finish?.let { DateTimeHelper.getShortDate(DateTime(it)) }}"

        tv_projectDetail_Days.text = "${project?.days} hari"

        val listDatess = project?.finish?.let { project.start?.let { it1 -> DateTimeHelper.generateDateBetween(it1, it) } }
        Log.d("LIST_DATES", "$listDatess")


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}