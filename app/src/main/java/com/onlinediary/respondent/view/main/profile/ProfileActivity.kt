package com.onlinediary.respondent.view.main.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.onlinediary.respondent.view.login.LoginActivity
import com.onlinediary.respondent.view.main.dashboard.DashboardActivity
import com.onlinediary.respondent.view.obtainViewModel
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class ProfileActivity : AppCompatActivity() {

    private val profileViewModel : ProfileViewModel by lazy {
        obtainViewModel()
    }

    private var profile : Profile?  = null

    private lateinit var isRefreshing : Disposable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        profile = intent.getParcelableExtra(DashboardActivity.DETAIL_PROFILE)

        if (profile!=null){
            populateData(profile)
        }

        isRefreshing = RxBus.listen(RxEvent.isRefreshing::class.java).subscribe {
            if (it.isRefresh){
                profileViewModel.getProfile()
            }
        }


        observeData()

        doLogout()




    }

    private fun doLogout() {
        //alert
        val alert = AlertDialog.Builder(this)
        alert.setCancelable(false)
        alert.setMessage("Sedang Logout...")
        val dialog : AlertDialog = alert.create()


        btn_logout.setOnClickListener {

            val alert = AlertDialog.Builder(this)
            alert.setCancelable(true)
            alert.setTitle("Logout?")
            alert.setMessage("Anda yakin ingin keluar dari aplikasi?")
            alert.setPositiveButton("YA"){dialogInterface, i ->
                dialogInterface.dismiss()
                dialog.show()
                profileViewModel.doLogout()

                Handler().postDelayed({
                    dialog.dismiss()
                    this.startActivity(Intent(this, LoginActivity::class.java))
                    finishAffinity()
                },1500)


            }

            alert.setNegativeButton("Tidak"){dialogInterface, i ->
                dialogInterface.dismiss()
            }

            val dialog : AlertDialog = alert.create()
            dialog.show()



        }

    }

    private fun observeData() {
        profileViewModel.profileItem.observe(this, Observer {
            if (it!=null){
                populateData(it)
            }
        })

        profileViewModel.progress.observe(this, Observer {

        })

        profileViewModel.error.observe(this, Observer {
            toast(it)
        })
    }

    private fun populateData(profile: Profile?) {
        tv_NamaProfile.text = profile?.name

        Glide.with(this)
            .load(BaseApiService.BASE_IMAGE_PROFILE_URL + profile?.photo)
            .into(iv_profile)

        et_email.setText(profile?.email)
        et_phone.setText(profile?.phone)
        et_birthDay.setText(profile?.birthday)
        et_gender.setText(profile?.gender)
        et_marital.setText(profile?.gender)


        btn_editProfile.setOnClickListener {
            this.startActivity<EditProfileActivity>(DETAIL_PROFILE to profile)
        }
    }


    private fun obtainViewModel(): ProfileViewModel = obtainViewModel(ProfileViewModel::class.java)


    companion object{
        const val DETAIL_PROFILE = "DETAIL_PROFILE"
    }
}