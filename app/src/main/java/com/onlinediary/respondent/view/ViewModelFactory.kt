package com.onlinediary.respondent.view

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.onlinediary.respondent.data.repository.MainRepository
import com.onlinediary.respondent.data.repository.local.LocalRepository
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.data.repository.remote.RemoteRepository
import com.onlinediary.respondent.view.login.LoginViewModel
import com.onlinediary.respondent.view.main.createpost.CreatePostViewModel
import com.onlinediary.respondent.view.main.dashboard.DashBoardViewModel
import com.onlinediary.respondent.view.main.profile.ProfileViewModel

class ViewModelFactory private constructor(
    private val application: Application,
    private val preferenceService: PreferenceService,
    private val mainRepository: MainRepository
) : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass){
            when{
                isAssignableFrom(LoginViewModel::class.java)->
                    LoginViewModel(preferenceService, mainRepository)
                isAssignableFrom(DashBoardViewModel::class.java)->
                    DashBoardViewModel(application, mainRepository, preferenceService)
                isAssignableFrom(CreatePostViewModel::class.java)->
                    CreatePostViewModel(application, mainRepository, preferenceService)
                isAssignableFrom(ProfileViewModel::class.java)->
                    ProfileViewModel(application, mainRepository, preferenceService)


                else-> error("invalid viewmodelclass")



            }

            as T


        }




    companion object{
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application): ViewModelFactory{
            return INSTANCE?: synchronized(ViewModelFactory::class.java){
                ViewModelFactory(application, PreferenceService(application), MainRepository.getInstance(
                    RemoteRepository(),
                    LocalRepository()
                ))
            }
        }
    }

}