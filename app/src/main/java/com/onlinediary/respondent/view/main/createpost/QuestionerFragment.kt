package com.onlinediary.respondent.view.main.createpost

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.gone
import com.onlinediary.respondent.utils.visible
import com.onlinediary.respondent.view.obtainViewModel
import kotlinx.android.synthetic.main.fragment_questioner.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class QuestionerFragment : Fragment() {


    companion object{
        const val FCR = 1

        const val ACT_DETAIL = "ACT_DETAIL"

        @JvmStatic
        fun newInstance(actDetail: ActivityList): QuestionerFragment {
            val bundle = Bundle().apply {
                putParcelable(ACT_DETAIL, actDetail)
            }

            return QuestionerFragment().apply {
                arguments = bundle
            }
        }


    }

    private var mCM: String? = null
    private var mUM: ValueCallback<Uri>? = null
    private var mUMA: ValueCallback<Array<Uri>>? = null



    private val createPostViewModel : CreatePostViewModel by lazy {
        obtainViewModel()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_questioner, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val actdetail = arguments?.getParcelable<ActivityList>(ACT_DETAIL)

        Log.d("ACT_DETAIL", "$actdetail")

        populateData(actdetail)




    }


    //main populatedata
    private fun populateData(act: ActivityList?) {
        val url = BaseApiService.BASE_URL + "qna/${act?.id}"


        webView_detail_act.isVerticalScrollBarEnabled = true
        webView_detail_act.isHorizontalScrollBarEnabled = true
        webView_detail_act.settings.javaScriptEnabled = true
        webView_detail_act.settings.domStorageEnabled = true
        webView_detail_act.settings.allowFileAccess = true
        webView_detail_act.settings.allowContentAccess = true
        webView_detail_act.settings.setSupportZoom(false)
        webView_detail_act.settings.useWideViewPort = true


        val headerMap = HashMap<String, String>()
        if (createPostViewModel.token != null) {
            headerMap["Authorization"] = createPostViewModel.token!!
        }

        webView_detail_act.webChromeClient = object : WebChromeClient() {
            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                if (mUMA != null) {
                    mUMA!!.onReceiveValue(null)
                }
                mUMA = filePathCallback
                var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent!!.resolveActivity(context?.packageManager!!) != null) {
                    var photoFile: File? = null
                    try {
                        photoFile = createImageFile()
                        takePictureIntent.putExtra("PhotoPath", mCM)
                    } catch (ex: IOException) {
                        Log.e("Webview", "Image file creation failed", ex)
                    }
                    if (photoFile != null) {
                        mCM = "file:" + photoFile.absolutePath
                        takePictureIntent.putExtra(
                                MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile)
                        )
                    } else {
                        takePictureIntent = null
                    }
                }
                val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                contentSelectionIntent.type = "*/*"
                val intentArray: Array<Intent>
                intentArray = takePictureIntent?.let { arrayOf(it) } ?: arrayOf<Intent>()
                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                startActivityForResult(chooserIntent, FCR)
                return true
            }


            override fun onProgressChanged(view: WebView?, newProgress: Int) {

                progress_webViewFragment.visible()
                progress_webViewFragment.progress = newProgress

                if (newProgress==100){
                    progress_webViewFragment.gone()
                }

                super.onProgressChanged(view, newProgress)

            }


        }
        webView_detail_act.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                //url will be the url that you click in your webview.
                //you can open it with your own webview or do
                //whatever you want

                //Here is the example that you open it your own webview.
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progress_webViewFragment.gone()
                super.onPageFinished(view, url)

            }
        }

        webView_detail_act.loadUrl(url, headerMap)
        //  webView_detail_act.postUrl(url, ByteArray(0))



    }


    // Create an image file
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        @SuppressLint("SimpleDateFormat") val timeStamp: String =
                SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "img_" + timeStamp + "_"
        val storageDir: File =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }
    fun openFileChooser(uploadMsg: ValueCallback<Uri?>?) {
        this.openFileChooser(uploadMsg, "*/*")
    }

    fun openFileChooser(
            uploadMsg: ValueCallback<Uri?>?,
            acceptType: String?
    ) {
        this.openFileChooser(uploadMsg, acceptType, null)
    }

    fun openFileChooser(
            uploadMsg: ValueCallback<Uri?>?,
            acceptType: String?,
            capture: String?
    ) {
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.addCategory(Intent.CATEGORY_OPENABLE)
        i.type = "*/*"
        this.startActivityForResult(Intent.createChooser(i, "File Browser"), FCR)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (Build.VERSION.SDK_INT >= 21) {
            var results: Array<Uri>? = null
            //Check if response is positive
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == FCR) {
                    if (null == mUMA) {
                        return
                    }
                    if (intent == null) { //Capture Photo if no image available
                        if (mCM != null) {
                            results = arrayOf(Uri.parse(mCM))
                        }
                    } else {
                        val dataString = intent.dataString
                        if (dataString != null) {
                            results = arrayOf(Uri.parse(dataString))
                        }
                    }
                }
            }
            mUMA!!.onReceiveValue(results)
            mUMA = null
        } else {
            if (requestCode == FCR) {
                if (null == mUM) return
                val result =
                        if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
                mUM!!.onReceiveValue(result)
                mUM = null
            }
        }
    }


    private fun obtainViewModel(): CreatePostViewModel = obtainViewModel(CreatePostViewModel::class.java)


}