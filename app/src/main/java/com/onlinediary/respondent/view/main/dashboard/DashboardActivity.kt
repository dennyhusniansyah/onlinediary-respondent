package com.onlinediary.respondent.view.main.dashboard

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.repository.local.entity.ProjectList
import com.onlinediary.respondent.data.response.ProjectListResponse
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.DateTimeHelper.dateFormatter
import com.onlinediary.respondent.utils.DateTimeHelper.getTodayDateAndDay
import com.onlinediary.respondent.utils.DateTimeHelper.getTodayDateTimeFlip
import com.onlinediary.respondent.utils.DateTimeHelper.historyDateFormatFLIP
import com.onlinediary.respondent.utils.ProgressLoading
import com.onlinediary.respondent.utils.datatransfer.RxBus
import com.onlinediary.respondent.utils.datatransfer.RxEvent
import com.onlinediary.respondent.utils.extension.isVisible
import com.onlinediary.respondent.utils.extension.makeStatusBarTransparent
import com.onlinediary.respondent.utils.gone
import com.onlinediary.respondent.utils.visible
import com.onlinediary.respondent.view.main.createpost.CreatePostActivity
import com.onlinediary.respondent.view.main.profile.ProfileActivity
import com.onlinediary.respondent.view.main.project.DetailProjectActivity
import com.onlinediary.respondent.view.main.project.ListProjectSheet
import com.onlinediary.respondent.view.obtainViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_dashboard.*
import net.danlew.android.joda.JodaTimeAndroid
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.*
import com.onlinediary.respondent.adapter.ActivityItem


class DashboardActivity : AppCompatActivity() {
    private val dashBoardViewModel: DashBoardViewModel by lazy {
        obtainViewModel()
    }

    private var mProject : ProjectList? = null
    private var mProfile : Profile? = null
    private var date : String =""

    //RX
    private lateinit var isRefreshing : Disposable
    private lateinit var mProjects : Disposable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        JodaTimeAndroid.init(this)
        makeStatusBarTransparent()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.container_dashboard)){ _, insets->
            findViewById<LinearLayout>(R.id.linear_top).setPadding(18,insets.systemWindowInsetTop,18,0)
            insets.consumeSystemWindowInsets()
        }

        dashBoardViewModel.getDatadashBoard()

        observeData()

        if (btn_reload_Dashboard.isVisible()){
            btn_reload_Dashboard.setOnClickListener {
                dashBoardViewModel.getDatadashBoard()
            }
        }

    }

    private fun observeData() {

        //list project
        dashBoardViewModel.listProject.observe(this, Observer {

            //jika list project kosong
            if (it?.isEmpty()!!){
                tv_projectName.text = "belum ada project"
                btn_addPost.setOnClickListener {
                    toast("anda belum terdaftar di project!")
                }

             //else, assign ke val mProject
            }else{
                mProject = it.first()
                tv_projectName.text = mProject?.name
                Log.d("FIRST_DATA", "$mProject")

                //getPost of project
                date = getTodayDateTimeFlip()
                tv_datePost.text = getTodayDateAndDay()
                mProject!!.id?.let { it1 -> dashBoardViewModel.getPostActiviyList(it1, date) }

                populateAction(it)
            }


        })

        //list activity
        dashBoardViewModel.listActivity.observe(this, Observer {
            if (it!=null){
                if (it.isNotEmpty()){
                    initReyclerView(it.toRecyclerListItem())
                    rv_listPost.visible()
                    layout_noData_dashboard.gone()
                }else{
                    rv_listPost.gone()
                    layout_noData_dashboard.visible()
                }
            }
        })


        //profile Detail
        dashBoardViewModel.profile.observe(this, Observer {
            mProfile = it
            Glide.with(this)
                .load(BaseApiService.BASE_IMAGE_PROFILE_URL + it?.photo)
                .into(iv_profileDashboard)


            //goto profile
            iv_profileDashboard.setOnClickListener {
                this.startActivity<ProfileActivity>(DETAIL_PROFILE to mProfile)
            }
        })

        dashBoardViewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING->{
                    layout_loading_Dashboard.visible()
                    rv_listPost.gone()
                    layout_noData_dashboard.gone()
                    btn_addPost.gone()
                    layout_noInternet_Dashboard.gone()
                }
                ProgressLoading.DONE->{
                    layout_loading_Dashboard.gone()
                    layout_noInternet_Dashboard.gone()
                    btn_addPost.visible()

                }
                ProgressLoading.ERROR->{
                    layout_loading_Dashboard.gone()
                    rv_listPost.gone()
                    btn_addPost.gone()
                    layout_noData_dashboard.gone()
                    layout_noInternet_Dashboard.visible()
                }
            }

        })


        dashBoardViewModel.error.observe(this, Observer {
            toast(it)
        })


        isRefreshing = RxBus.listen(RxEvent.isRefreshing::class.java).subscribe {
            if (it.isRefresh){
                mProject!!.id?.let { it1 -> dashBoardViewModel.getPostActiviyList(it1, date) }
            }
        }

        mProjects = RxBus.listen(RxEvent.sendProjectItem::class.java).subscribe{
            mProject = it.project
            tv_projectName.text = it.project.name
            mProject!!.id?.let { it1 -> dashBoardViewModel.getPostActiviyList(it1, date) }
            Log.d("PROJECT_CHANGE", "$mProject")
            toast("project change")
        }

    }


    private fun populateAction(projectListResponse: ProjectListResponse?) {
        val dialogBuilder = AlertDialog.Builder(this)
        val projectOption = arrayOf(DETAIL_PROJECT, LIST_PROJECT)

        //project click
        layout_DashboardlistProject.setOnClickListener {
            dialogBuilder.setItems(projectOption){ _, p1 ->
                when(p1){

                    //detail project
                    0->{
                        Log.d("DETAIL_PROJECT_BEFORE", "$mProject")
                        this.startActivity<DetailProjectActivity>(DETAIL_PROJECT to mProject)
                    }

                    //list project
                    1->{
                        val listProjectSheet = projectListResponse?.let { it1 ->
                            ListProjectSheet.newInstance(it1) }

                        listProjectSheet?.show(supportFragmentManager, "LIST_PROJECT")


                    }
                }
            }

            val dialog = dialogBuilder.create()
            dialog.show()

        }

        //add post
        btn_addPost.setOnClickListener {
            this.startActivity<CreatePostActivity>(CreatePostActivity.ID_PROJECT to mProject?.id)
        }



        layout_datePicker.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                c.set(Calendar.YEAR, year)
                c.set(Calendar.MONTH, monthOfYear)
                c.set(Calendar.DAY_OF_MONTH,dayOfMonth)

                tv_datePost.text = dateFormatter.print(c.timeInMillis)
                date = historyDateFormatFLIP.print(c.timeInMillis)

                //get List Activity on click
                mProject?.id?.let { it1 -> dashBoardViewModel.getPostActiviyList(it1, date) }

            }, year, month, day)


            dpd.datePicker.maxDate = Date().time
            dpd.show()

        }


    }


    private fun initReyclerView(items : List<ActivityItem>){
        val  groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(items)
        }
        groupAdapter.notifyDataSetChanged()

        rv_listPost.apply {
            adapter = groupAdapter
        }
    }

    private fun List<ActivityList>.toRecyclerListItem() : List<ActivityItem>{
        return this.map { activitys->
            ActivityItem(activitys)
        }
    }

    private fun obtainViewModel(): DashBoardViewModel = obtainViewModel(DashBoardViewModel::class.java)

    companion object{
        const val DETAIL_PROJECT = "Detail Project"
        const val DETAIL_PROFILE = "Detail Profile"
        const val LIST_PROJECT = "List Project"
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isRefreshing.isDisposed) isRefreshing.dispose()
        if (!mProjects.isDisposed) mProjects.dispose()


    }

//    override fun onCommentClick(activity: ActivityList) {
//        val commentsFragment = CommentFragment.newInstance(activity)
//        commentsFragment.show(supportFragmentManager,"COMMENT_SHEET")
//    }


}