package com.onlinediary.respondent.adapter

import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ProjectList
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.layout_list_project.view.*

class ProjectItem (private val project : ProjectList, private val listener: ProjectListListener): Item(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView){
            tv_projectName.text = project.name
            tv_projectCode.text = project.code
            tv_projectTask.text = project.task
            tv_projectDeadline.text = "Deadline ${project.days} Hari"
//            tv_projectDays.text = "${ project.start } - ${ project.finish}"

            setOnClickListener {
                listener.onProjectClick(project)
            }
        }

    }

    override fun getLayout(): Int = R.layout.layout_list_project


    interface ProjectListListener{
        fun onProjectClick(project: ProjectList)
    }

}