package com.onlinediary.respondent.adapter

import com.bumptech.glide.Glide
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.CommentItems
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.DateTimeHelper
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.layout_comment.view.*
import org.joda.time.DateTime

class CommentItem (private val comment : CommentItems) : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView){

            tv_nameComment.text = comment.name
            tv_textComment.text = comment.post
            tv_dateComment.text = DateTimeHelper.getDatewithHour(DateTime(comment.createdAt))

            if (comment.photo!=null){
                Glide.with(context)
                    .load(BaseApiService.BASE_IMAGE_PROFILE_URL + comment.photo)
                    .into(iv_comment)
            }

        }
    }

    override fun getLayout(): Int = R.layout.layout_comment
}