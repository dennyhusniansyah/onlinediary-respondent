package com.onlinediary.respondent.adapter

import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.network.BaseApiService
import com.onlinediary.respondent.utils.DateTimeHelper
import com.onlinediary.respondent.view.WebViewActivity
import com.onlinediary.respondent.view.main.createpost.DetailPostActivity
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.layout_list_post_item.view.*
import org.jetbrains.anko.startActivity
import org.joda.time.DateTime


class ActivityItem (private val activitys: ActivityList) : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView){
            tv_captionPost.text = activitys.name
            //  tv_namePost.text = activitys.user
            tv_datePost.text = DateTimeHelper.getDatewithHour(DateTime(activitys.createdAt))

            tv_commentCount.text = "${activitys.comment} komentar"



            //     tv_statusPost.text = activitys.status

            when(activitys.status){
                "pending"->{
                    iv_statusPost.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_warning))

                }
                "approved"->{
                    iv_statusPost.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_checklist))

                }
                "rejected"->{
                    iv_statusPost.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_rejected))

                }
            }

            when(activitys.qnaire) {
                0 -> {
                    btn_gotoKuesioner.text = "Tidak Ada Kuesioner"
                }
                else -> {
                    btn_gotoKuesioner.text = "Isi Kuesioner"

                    btn_gotoKuesioner.setOnClickListener {
                        context.startActivity<WebViewActivity>(WebViewActivity.ID_ACTIVITY_FROM_NOTIF to activitys.id.toString())

                    }
                }
            }

            val newHeight = context.resources.displayMetrics.widthPixels

                iv_imagePost.layoutParams.height = newHeight


            Glide.with(context)
                .load(BaseApiService.BASE_IMAGE_ACTIVITY_URL + activitys.photo)
                .into(iv_imagePost)

//            Glide.with(context)
//                .load(BaseApiService.BASE_IMAGE_PROFILE_URL + activitys.userPhoto)
//                .into(iv_imageProfilePost)


//
//            layout_comment.setOnClickListener {
//               listener.onCommentClick(activitys)
//            }

            layout_comment.setOnClickListener {
                context.startActivity<DetailPostActivity>(DetailPostActivity.ID_POST to activitys.id.toString())
            }

        }

    }

    override fun getLayout(): Int = R.layout.layout_list_post_item


    interface ActivityListener{
        fun onCommentClick(activity : ActivityList)
    }

}
