package com.onlinediary.respondent.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.*
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import androidx.annotation.IntDef
import androidx.core.content.ContextCompat
import com.onlinediary.respondent.R
import com.onlinediary.respondent.utils.extension.*
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.INPUT_TYPE_NAME
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.INPUT_TYPE_NUMBER
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_ADDRESS
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_CAPWORDS
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_DECIMAL
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_EMAIL
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_NAME
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_NUMBER
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_PASSWORD
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_PHONE
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_RUPIAH
import com.onlinediary.respondent.widgets.WaroengTextFieldExtra.Companion.TYPE_TEXT
import kotlinx.android.synthetic.main.widget_waroeng_text_field.view.*
import org.jetbrains.anko.singleLine

class WaroengTextField : LinearLayout {

    private var enableField: Boolean = true
    private var showCounter: Boolean = false
    private var enableFocusable: Boolean = true
    private var fieldType: Int = TYPE_TEXT
    private var label: String? = null
    private var hint: String? = null
    private var maxLength: Int = 0
    private var minLength: Int = 0
    private var maxLines: Int = 0
    private var imeOption: Int = EditorInfo.IME_ACTION_NEXT
    private var leftIcon: Drawable? = null
    private var textFieldBackground: Drawable? = null

    var textWatcher: WaroengTextFieldTextWatcher? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initWithAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initWithAttrs(attrs)
    }

    private fun initWithAttrs(attrs: AttributeSet) {
        val attributeSet = context.obtainStyledAttributes(attrs, R.styleable.WaroengTextField)
        enableField = attributeSet.getBoolean(R.styleable.WaroengTextField_isEnable, enableField)
        showCounter = attributeSet.getBoolean(R.styleable.WaroengTextField_showCounter, showCounter)
        enableFocusable = attributeSet.getBoolean(R.styleable.WaroengTextField_isFocusableEnable, enableFocusable)
        fieldType = attributeSet.getInt(R.styleable.WaroengTextField_fieldType, fieldType)
        label = attributeSet.getString(R.styleable.WaroengTextField_label)
        hint = attributeSet.getString(R.styleable.WaroengTextField_hintLabel)
        maxLength = attributeSet.getInt(R.styleable.WaroengTextField_maxLength, maxLength)
        imeOption = attributeSet.getInt(R.styleable.WaroengTextField_imeOption, imeOption)
        minLength = attributeSet.getInt(R.styleable.WaroengTextField_minLength, minLength)
        maxLines = attributeSet.getInt(R.styleable.WaroengTextField_maxLines, maxLines)
        leftIcon = attributeSet.getDrawable(R.styleable.WaroengTextField_leftIcon)
        textFieldBackground = attributeSet.getDrawable(R.styleable.WaroengTextField_textFieldBackground)
        attributeSet.recycle()

        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.widget_waroeng_text_field, this)

        setFieldType(fieldType)
        setLabel(label)
        setHint(hint)
        setFieldFocusable(enableFocusable)
        setEnableField(enableField)
        setMaxLength(maxLength)
        setMinLength(minLength)
        setMaxLines(maxLines)
        setLeftIcon(leftIcon)
        setImeOption(imeOption)
        setTextCounter(text_field_edit_text.text.toString())

        setOnTextChangeListener()
    }

    private fun setFieldFocusable(isFocusable: Boolean) {
        text_field_edit_text.isFocusable = isFocusable
    }

    private fun setImeOption(imeOption: Int) {
        this.imeOption = imeOption
        text_field_edit_text.imeOptions = imeOption
    }

    private fun setFieldType(@WaroengTextFieldExtra.FieldTypeDef fieldType: Int) {
        this.fieldType = fieldType

        when (fieldType) {
            TYPE_CAPWORDS -> {
                text_field_edit_text.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                text_field_edit_text.maxLines = 1
                text_field_edit_text.singleLine = true
            }
            TYPE_TEXT -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            }
            TYPE_EMAIL -> {
                text_field_edit_text.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            }
            TYPE_NUMBER -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_NUMBER
                text_field_edit_text.keyListener = DigitsKeyListener.getInstance(INPUT_TYPE_NUMBER)
            }
            TYPE_ADDRESS -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            }
            TYPE_PASSWORD -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
            TYPE_DECIMAL -> {
                text_field_edit_text.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_CLASS_NUMBER
            }
            TYPE_RUPIAH -> {
                text_field_edit_text.keyListener = DigitsKeyListener.getInstance("0123456789")
                text_field_edit_text.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_CLASS_NUMBER
                setMaxLength(17)
            }
            TYPE_PHONE -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_PHONE
                text_field_edit_text.keyListener = DigitsKeyListener.getInstance(INPUT_TYPE_NUMBER)
            }
            TYPE_NAME -> {
                text_field_edit_text.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_WORDS or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            }
            else -> {
                text_field_edit_text.inputType = InputType.TYPE_CLASS_TEXT
            }
        }
    }

    private fun setLabel(label: String?) {
        this.label = label

        if (label.isNullOrEmpty()) {
            text_field_label.makeGone()
        } else {
            text_field_label.makeVisible()
        }
        text_field_label.text = label
    }

    private fun setHint(hint: String?) {
        this.hint = hint
        text_field_edit_text.hint = hint
    }

    private fun setEnableField(enable: Boolean) {
        this.enableField = enable
        if (enable) {
            text_field_edit_text.makeEnable()
            container_field.isClickable = false
        } else {
            text_field_edit_text.makeDisable()
            container_field.isClickable = false
            container_field.makeDisable()
        }
    }

    private fun setTextCounter(text: String) {
        when {
            !showCounter -> {
                text_field_counter.makeGone()
            }
            minLength > 0 -> {
                text_field_counter.makeVisible()
                text_field_counter.text = getTextCounter(text)
            }
            maxLength > 0 -> {
                text_field_counter.makeVisible()
                text_field_counter.text = getTextCounter(text).plus("/").plus(maxLength)
            }
            else -> {
                text_field_counter.makeGone()
            }
        }
    }

    private fun getTextCounter(text: String): String {
        if (text.length > MAX_COUNTER) return MAX_COUNT_NUMBER

        return text.length.toString()
    }

    fun showError(message: String?) {
        /*text_field_error.text = message
        text_field_error.visibility = View.VISIBLE*/
        text_field_input_layout.background = ContextCompat.getDrawable(context, R.drawable.bg_shape_rectangle_ed_text_field_error)
        this.refreshDrawableState()
    }

    fun hideError() {
        /*text_field_error.text = ""
        text_field_error.visibility = View.INVISIBLE*/
        text_field_input_layout.background = ContextCompat.getDrawable(context, R.drawable.bg_shape_rectangle_ed_text_field_initial)
        this.refreshDrawableState()
    }

    private fun setMaxLength(maxLength: Int) {
        this.maxLength = maxLength
        setFilters(fieldType, this.maxLength)
    }

    private fun setFilters(fieldType: Int, maxLength: Int) {
        val filters = mutableListOf<InputFilter>()

        if (maxLength > 0) {
            filters.add(InputFilter.LengthFilter(maxLength))
        }
        if (fieldType == TYPE_NAME) {
            filters.add(nameInputFilter())
        }

        text_field_edit_text.filters = filters.toTypedArray()
    }

    private fun nameInputFilter(): InputFilter {
        return object : InputFilter {
            override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence {
                if (source.isNullOrBlank()) return source ?: ""
                if (source.toString().matches(Regex(INPUT_TYPE_NAME))) return source
                return ""
            }
        }
    }

    private fun setMinLength(minLength: Int) {
        this.minLength = minLength
    }

    private fun setMaxLines(maxLines: Int) {
        this.maxLines = maxLines
        if (maxLines > 0) {
            this.maxLines = maxLines
            if (maxLines == 1 && fieldType != TYPE_PASSWORD) {
                text_field_edit_text.setSingleLine(true)
            }
        }
    }

    private fun setLeftIcon(drawable: Drawable?) {
        text_field_edit_text.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
        if (drawable != null) {
            text_field_edit_text.compoundDrawablePadding = resources.getDimensionPixelSize(R.dimen.medium_margin)
        }
    }

    private fun setOnTextChangeListener() {
        if (fieldType == TYPE_RUPIAH) {

            val textWatcher = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    try {
                        val value = s.toString().toRealAmount().toString()
                        if (!value.isBlank()) {
                            val formattedAmount = value.toDouble().toFormattedAmount()
                            text_field_edit_text.removeTextChangedListener(this)
                            text_field_edit_text.setText(formattedAmount)
                            text_field_edit_text.setSelection(formattedAmount.length)
                            text_field_edit_text.addTextChangedListener(this)
                            text_field_edit_text.setSelection(formattedAmount.length)
                        } else {
                            text_field_edit_text.removeTextChangedListener(this)
                            text_field_edit_text.setText(0.0.toFormattedAmount())
                            text_field_edit_text.addTextChangedListener(this)
                            text_field_edit_text.setSelection(s.toString().length)
                        }
                    } catch (e: NumberFormatException) {
                        text_field_edit_text.setSelection(s.toString().length)
                    } catch (e: IndexOutOfBoundsException) {
                        text_field_edit_text.setSelection(s.toString().length - 1)
                    }
                }

                override fun afterTextChanged(s: Editable?) {
                    val rawValue = s.toString().toRealAmount().toString()
                    this@WaroengTextField.textWatcher?.whenValueChanged(Editable.Factory.getInstance().newEditable(rawValue))
                }
            }
            text_field_edit_text.addTextChangedListener(textWatcher)
        } else {
            val textWatcher = object : TextWatcher {
                override fun afterTextChanged(input: Editable?) {
                    input?.run {
                        setTextCounter(this.toString())
                    }
                    this@WaroengTextField.textWatcher?.whenValueChanged(input)
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {}
            }
            text_field_edit_text.addTextChangedListener(textWatcher)
        }
    }

    fun addTextChangedListener(watcher: TextWatcher) {
        text_field_edit_text.addTextChangedListener(watcher)
    }

    fun setOnFocusChangedListener(listener: View.OnFocusChangeListener?) {
        if (listener != null) {
            text_field_edit_text.onFocusChangeListener = listener
        }
    }

    fun requestFocusOnTextField() {
        text_field_edit_text.requestFocus()
    }

    fun setValue(value: String) {
        text_field_edit_text.setText(value)
    }

    override fun setOnClickListener(listener: OnClickListener?) {
        text_field_edit_text.isClickable = false
        text_field_edit_text.isFocusable = false
        text_field_edit_text.clearFocus()
        text_field_edit_text.setOnClickListener(listener)
    }

    fun clearText() {
        text_field_edit_text.clearText()
    }

    fun getValue(): String {
        return text_field_edit_text.text.toString()
    }

    companion object {
        const val MAX_COUNTER = 1000
        const val MAX_COUNT_NUMBER = "999+"
    }
}

interface WaroengTextFieldTextWatcher {
    fun whenValueChanged(s: Editable?)
}

class WaroengTextFieldExtra {
    @IntDef(
        TYPE_TEXT,
        TYPE_EMAIL,
        TYPE_NUMBER,
        TYPE_NAME,
        TYPE_PASSWORD,
        TYPE_PHONE,
        TYPE_DECIMAL,
        TYPE_CAPWORDS
    )
    annotation class FieldTypeDef

    companion object {
        const val TYPE_TEXT = 1
        const val TYPE_EMAIL = 2
        const val TYPE_NUMBER = 3
        const val TYPE_NAME = 4
        const val TYPE_ADDRESS = 5
        const val TYPE_PASSWORD = 6
        const val TYPE_DECIMAL = 7
        const val TYPE_PHONE = 8
        const val TYPE_CAPWORDS = 9
        const val TYPE_RUPIAH = 10

        const val INPUT_TYPE_NUMBER = "0123456789"
        const val INPUT_TYPE_NAME = "[a-zA-Z ]+"
    }
}