package com.onlinediary.respondent.network

import com.onlinediary.respondent.BuildConfig
import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.entity.Profile
import com.onlinediary.respondent.data.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface BaseApiService{
    companion object {
        const val BASE_URL = BuildConfig.BASE_URL
        const val BASE_IMAGE_ACTIVITY_URL = "https://onlinediary.well-done.id/upload/activity/"
        const val BASE_IMAGE_PROFILE_URL = "https://onlinediary.well-done.id/upload/profile/"


        /***POST METHODE***/
        const val POST_LOGIN = "login"
        const val POST_ACTIVITY = "post-activity"
        const val UPDATE_PROFILE_RESP = "update-profile"
        const val POST_COMMENT = "send-comment/{id_post}"


        /***GET METHODE***/
        const val GET_PROJECT_LIST = "task-list"
        const val GET_ACTIVITY_LIST = "list-activity/{id_project}/{date}"
        const val GET_PROFILE = "profile"
        const val GET_COMMENT_POST = "comments/{id_post}"
        const val GET_DETAILACT_BYID = "detail-activity/{id_post}"


    }

    /*========================POST===========================*/


    @POST(POST_LOGIN)
    @FormUrlEncoded
    suspend fun postLogin(@Field("email") email: String,
                          @Field("fcm_token") fcmToken: String,
                          @Field("password") password: String): LoginResponse

    @POST(POST_ACTIVITY)
    @Multipart
    suspend fun postActivity(
        @Header("Authorization") accessToken: String,
        @Part photo : MultipartBody.Part?,
        @Part("name") name : RequestBody?,
        //@Part("story") story : RequestBody?,
        @Part("projects_id") projectId : Int?,
        @Part("used") usedProduct : RequestBody? //yes or no
        ):GeneralPostResponse


    @POST(UPDATE_PROFILE_RESP)
    @Multipart
    suspend fun updateProfileResp(
            @Header("Authorization") accessToken: String,
            @Part photo : MultipartBody.Part?,
            @Part("name") name : RequestBody?,
            @Part("email") email : RequestBody?,
            @Part("phone") phone : RequestBody?,
            @Part("birthday") birthday : RequestBody?,
            @Part("gender") gender : RequestBody?,
            @Part("marital") marital : RequestBody?,
            @Part("password") password : RequestBody
    ) : GeneralPostResponse

    @POST(POST_COMMENT)
    @FormUrlEncoded
    suspend fun postComment(
        @Header ("Authorization") accesToken : String,
        @Field("comment") comment: String?,
        @Path("id_post") idPost: Int?


        ): GeneralPostResponse




    /*========================GET===========================*/
    @GET(GET_PROJECT_LIST)
    suspend fun getListProject(
        @Header ("Authorization") accesToken : String
    ): ProjectListResponse

    @GET(GET_ACTIVITY_LIST)
    suspend fun getActivityList(
        @Header("Authorization") accessToken: String,
        @Path("id_project") idProj: Int,
        @Path("date") dates: String
        ):ActivityListResponse?

    @GET(GET_PROFILE)
    suspend fun getProfile(
        @Header("Authorization") accessToken: String

    ): Profile


    @GET(GET_COMMENT_POST)
    suspend fun getCommentPost(
        @Header ("Authorization") accesToken : String,
        @Path("id_post") id: Int
    ): CommentResponse


    @GET(GET_DETAILACT_BYID)
    suspend fun getDetailActById(
        @Header ("Authorization") accesToken : String,
        @Path("id_post") id: Int
    ): ActivityList


}