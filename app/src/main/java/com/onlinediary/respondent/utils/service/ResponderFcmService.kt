package com.onlinediary.respondent.utils.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import androidx.core.app.NotificationCompat
import com.onlinediary.respondent.R
import com.onlinediary.respondent.data.repository.local.LocalServiceManager
import com.onlinediary.respondent.data.repository.local.preference.PreferenceService
import com.onlinediary.respondent.view.WebViewActivity
import com.onlinediary.respondent.view.login.LoginActivity
import com.onlinediary.respondent.view.main.createpost.CreatePostActivity
import com.onlinediary.respondent.view.main.createpost.DetailPostActivity
import com.onlinediary.respondent.view.main.dashboard.DashboardActivity

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class ResponderFcmService : FirebaseMessagingService() {
    companion object{
        private val TAG = ResponderFcmService::class.java.simpleName
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d(TAG, "Refreshed token: $p0")

    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        val idActivity = p0.data["id_activity"]
        val idProject = p0.data["project_id"]
        val notif = p0.notification

        sendNotification(notif, idActivity, idProject)


    }



    private fun sendNotification(notif: RemoteMessage.Notification?, idActivity: String?, idProject: String?) {
        val channelId = getString(R.string.default_notification_channel_id)
        val channelName = getString(R.string.default_notification_channel_name)

        var intent: Intent

        val preferenceService = PreferenceService(this)
        val localServiCeManager = LocalServiceManager(preferenceService)
        val session = localServiCeManager.isSessionActive()

        when (notif?.title) {
            "Questionnaire" -> {
                if (!session){
                    intent = Intent(this, LoginActivity::class.java)
                }else {
                    intent = Intent(this, WebViewActivity::class.java)
                    intent.putExtra(WebViewActivity.ID_ACTIVITY_FROM_NOTIF, idActivity)
                }
            }
            "Comments" -> {
                if (!session){
                    intent = Intent(this, LoginActivity::class.java)
                }else {
                    intent = Intent(this, DetailPostActivity::class.java)
                    intent.putExtra(DetailPostActivity.ID_POST, idActivity)
                }


            }
            "Activity" ->{
                if (!session){
                    intent = Intent(this, LoginActivity::class.java)
                }else {
                    intent = Intent(this, CreatePostActivity::class.java)
                    intent.putExtra(CreatePostActivity.ID_PROJECT, idProject?.toInt())
                }
            }

            else -> {
                intent = if (!session){
                    Intent(this, LoginActivity::class.java)
                }else {
                    Intent(this, DashboardActivity::class.java)
                }
            }
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.wd_logo_only)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.wd_logo_big))
                .setContentText(notif?.body)
                .setContentTitle(notif?.title)
                .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setContentIntent(pendingIntent)
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE))
            val att = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()

            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            channel.enableVibration(true)
            channel.setSound(defaultSoundUri, att)
            channel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            channel.shouldVibrate()


            notificationBuilder.setChannelId(channelId)
            mNotificationManager.createNotificationChannel(channel)
        }
        val notification = notificationBuilder.build()
        mNotificationManager.notify(0, notification)
    }




}