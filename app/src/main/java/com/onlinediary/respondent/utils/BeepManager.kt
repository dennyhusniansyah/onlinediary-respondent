package com.onlinediary.respondent.utils


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Vibrator
import android.util.Log
import com.onlinediary.respondent.R
import java.io.IOException


class BeepManager(activity: Activity) {
    private val context: Context

    /**
     * Call updatePrefs() after setting this.
     *
     * If the device is in silent mode, it will not beep.
     *
     * @param beepEnabled true to enable beep
     */
    var isBeepEnabled = true

    /**
     * Call updatePrefs() after setting this.
     *
     * @param vibrateEnabled true to enable vibrate
     */
    var isVibrateEnabled = false

    @SuppressLint("MissingPermission")
    @Synchronized
    fun playBeepSoundAndVibrate() {
        if (isBeepEnabled) {
            playBeepSound()
        }
        if (isVibrateEnabled) {
            val vibrator =
                context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(VIBRATE_DURATION)
        }
    }

    private fun playBeepSound(): MediaPlayer? {
        val mediaPlayer = MediaPlayer()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mediaPlayer.setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
        }


        mediaPlayer.setOnCompletionListener { mp ->
            mp.stop()
            mp.release()
        }
        mediaPlayer.setOnErrorListener { mp, what, extra ->
            Log.w(
                TAG,
                "Failed to beep $what, $extra"
            )
            // possibly media player error, so release and recreate
            mp.stop()
            mp.release()
            true
        }
        return try {
            val file =
                context.resources.openRawResourceFd(R.raw.zxing_beep)
            file.use { file ->
                mediaPlayer.setDataSource(
                    file.fileDescriptor,
                    file.startOffset,
                    file.length
                )
            }
            mediaPlayer.setVolume(
                BEEP_VOLUME,
                BEEP_VOLUME
            )
            mediaPlayer.prepare()
            mediaPlayer.start()
            mediaPlayer
        } catch (ioe: IOException) {
            Log.w(TAG, ioe)
            mediaPlayer.release()
            null
        }
    }

    companion object {
        private val TAG = BeepManager::class.java.simpleName
        private const val BEEP_VOLUME = 0.10f
        private const val VIBRATE_DURATION = 200L
    }

    init {
        activity.volumeControlStream = AudioManager.STREAM_MUSIC

        // We do not keep a reference to the Activity itself, to prevent leaks
        context = activity.applicationContext
    }
}
