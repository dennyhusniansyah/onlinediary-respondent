package com.onlinediary.respondent.utils

import android.util.Base64
import com.onlinediary.respondent.BuildConfig.ENCRYPT_KEY
import java.security.Security
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import kotlin.text.Charsets.UTF_8

object EncryptHelper {
    private const val ALGORITHM = "AES/ECB/PKCS5Padding"

    fun encrypt(data: String): String {
        Security.addProvider(org.bouncycastle.jce.provider.BouncyCastleProvider())
        val seedValue = ENCRYPT_KEY.toByteArray()
        val secretKey = SecretKeySpec(seedValue, "AES")
        return try {
            Logger.debug("RAW ENCRYPT VALUE $data\nKEY >>> ${seedValue.toString(UTF_8)}")
            val cipher = Cipher.getInstance(ALGORITHM, "BC")
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
            val cipherText = cipher.doFinal(data.toByteArray(charset("UTF8")))
            Logger.debug("RAW ENCRYPT RESULT ${String(Base64.encode(cipherText, Base64.DEFAULT))}")
            String(Base64.encode(cipherText, Base64.DEFAULT))
        } catch (e: Exception) {
            Logger.debug("RAW ENCRYPT EXCEPTION $e")
            "{ }"
        }
    }

    fun decrypt(data: String?): String {
        Security.addProvider(org.bouncycastle.jce.provider.BouncyCastleProvider())
        val seedValue = ENCRYPT_KEY.toByteArray(charset("UTF8"))
        val secretKey = SecretKeySpec(seedValue, "AES")
        return try {
            Logger.debug("RAW DECRYPT VALUE $data\nKEY >>> ${seedValue.toString(UTF_8)}")
            val cipher = Cipher.getInstance(ALGORITHM, "BC")
            cipher.init(Cipher.DECRYPT_MODE, secretKey)
            val cipherText = Base64.decode(data?.toByteArray(charset("UTF8")), Base64.DEFAULT)
            Logger.debug("RAW DECRYPT RESULT ${String(cipher.doFinal(cipherText), UTF_8)}")
            String(cipher.doFinal(cipherText), UTF_8)
        } catch (e: Exception) {
            Logger.debug("RAW DECRYPT EXCEPTION $e")
            "{ }"
        }
    }

}