package com.onlinediary.respondent.utils.enumerated

enum class TransferItemStatus {
    ACCEPT,
    REJECT;

    companion object {
        fun transferItemStatus(type: TransferItemStatus): String = if (type == ACCEPT) "accept" else "reject"
    }
}