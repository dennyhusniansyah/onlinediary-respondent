package com.onlinediary.respondent.utils.model

interface Emptyable {

    fun isEmpty(): Boolean
}