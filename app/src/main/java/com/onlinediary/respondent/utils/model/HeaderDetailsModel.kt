package com.onlinediary.respondent.utils.model

import androidx.annotation.IntDef
import java.io.Serializable

open class HeaderDetailsModel(@ItemTypeDef open val type: Int) : Serializable {

    @IntDef(TYPE_HEADER, TYPE_DETAILS)
    annotation class ItemTypeDef

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_DETAILS = 1
    }
}