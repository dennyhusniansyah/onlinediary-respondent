package com.onlinediary.respondent.utils.datatransfer

import com.onlinediary.respondent.data.repository.local.entity.ActivityList
import com.onlinediary.respondent.data.repository.local.entity.ProjectList


class RxEvent {
    data class GetStatusToSend(val send : String)
    data class isRefreshing(val isRefresh : Boolean)
    data class sendProjectItem(val project : ProjectList)
    data class sendActivityDetail(val act : ActivityList)


}