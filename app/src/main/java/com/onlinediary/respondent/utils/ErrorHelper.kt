package com.onlinediary.respondent.utils

import android.content.Context
import com.onlinediary.respondent.R
import com.onlinediary.respondent.utils.extension.isServerRequestErrorNetwork
import com.onlinediary.respondent.utils.extension.isServerRequestErrorNoInternet
import com.onlinediary.respondent.utils.extension.isServerRequestErrorUnauthorized

object ErrorHelper {

    fun createErrorMessage(context: Context, throwable: Throwable): String {
        var errorMessage = ""
        when {
            throwable.isServerRequestErrorNetwork() -> {
                errorMessage = context.getString(R.string.error_network_message)
            }
            throwable.isServerRequestErrorNoInternet() -> {
                errorMessage = context.getString(R.string.error_network_message)
            }
            throwable.isServerRequestErrorUnauthorized() -> {
                errorMessage = context.getString(R.string.error_code_401)
            }
            else -> {
                errorMessage = context.getString(R.string.error_general)
            }
        }

        return errorMessage
    }

    fun createCredentialFailedMessage(context: Context, message: String?): String {
        return message ?: context.getString(R.string.error_default)
    }
}