package com.onlinediary.respondent.utils.extension

import android.util.Patterns

fun String.toAuthorization(): String {
    return "Bearer $this"
}

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPhoneNumber(): Boolean {
    return Patterns.PHONE.matcher(this).matches()
}

fun String?.toRealAmount(): Int {
    return if (this != null) {
        val amountBuilder = this.toLowerCase()
            .replace("rp. ", "")
            .replace("idr ", "")
            .replace("r", "")
            .replace("p", "")
            .replace("i", "")
            .replace("d", "")
            .replace(" ", "")
            .replace(".", "")
            .replace(",", "")
            .trim()
        if (amountBuilder.isNullOrBlank().not()) amountBuilder.toInt() else 0
    } else {
        0
    }
}

fun String?.toCapitalSentence(): String {
    return if (this != null) {
        if (this.isNullOrBlank()) {
            ""
        } else {
            val caps = Character.toUpperCase(this.toCharArray()[0]) + this.substring(1)
            when {
                caps.contains("dki", true) -> caps.replace("Dki", "DKI")
                caps.contains("di yogya", true) -> caps.replace("di yogya", "DI Yogya")
                else -> caps
            }
        }
    } else {
        ""
    }
}