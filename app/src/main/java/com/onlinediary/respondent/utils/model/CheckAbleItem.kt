package com.onlinediary.respondent.utils.model

import java.io.Serializable

open class CheckAbleItem<out T>(open val data: T?,
                                open var isChecked: Boolean) : Serializable {
}