package com.onlinediary.respondent.utils.extension

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.net.ConnectivityManager
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.onlinediary.respondent.R
import com.onlinediary.respondent.utils.Helper
import retrofit2.HttpException

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.isNetworkAvailable(): Boolean {
    val activeNetworkInfo = getSystemService<ConnectivityManager>(Context.CONNECTIVITY_SERVICE)?.activeNetworkInfo
    return activeNetworkInfo?.isConnected ?: false
}

inline fun <reified T> Context.getSystemService(systemService: String): T? {
    return getSystemService(systemService) as T
}

fun Context.showKeyboard() {
    val inputMethodManager = getSystemService<InputMethodManager>(Context.INPUT_METHOD_SERVICE)
    inputMethodManager?.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun Context.hideKeyboard(view: View) {
    val inputManager = getSystemService<InputMethodManager>(Context.INPUT_METHOD_SERVICE)
    inputManager?.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.forceHideKeyboard() {
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
}

fun Context.getScreenSize(): IntArray {
    val widthHeight = IntArray(2)
    widthHeight[Helper.WIDTH_INDEX] = 0
    widthHeight[Helper.HEIGHT_INDEX] = 0

    val windowManager = this.getSystemService<WindowManager>(Context.WINDOW_SERVICE)
    val display = windowManager?.defaultDisplay

    val size = Point()
    display?.getSize(size)
    widthHeight[Helper.WIDTH_INDEX] = size.x
    widthHeight[Helper.HEIGHT_INDEX] = size.y

    if (!Helper.isScreenSizeRetrieved(widthHeight)) {
        val metrics = DisplayMetrics()
        display?.getMetrics(metrics)
        widthHeight[0] = metrics.widthPixels
        widthHeight[1] = metrics.heightPixels
    }

    // Last defense. Use deprecated API that was introduced in lower than API 13
    if (!Helper.isScreenSizeRetrieved(widthHeight)) {
        widthHeight[0] = display?.width ?: 0 // deprecated
        widthHeight[1] = display?.height ?: 0 // deprecated
    }

    return widthHeight
}

fun Context.parseErrorThrowableToMessage(throwable: Throwable): String {
    if (throwable is HttpException) {
        val code = throwable.code()
        return when (code) {
            400 -> getString(R.string.error_code_400) // Bad Request
            401 -> getString(R.string.error_code_401) // Unauthorized
            403 -> getString(R.string.error_code_403) // Forbidden Error
            404 -> getString(R.string.error_code_404) // Not Found
            405 -> getString(R.string.error_code_405) // Method Not Allowed
            407 -> getString(R.string.error_code_407) // Proxy Authentication Required
            408 -> getString(R.string.error_code_408) // Request Timeout
            500 -> getString(R.string.error_code_500) // Internal Server Error
            502 -> getString(R.string.error_code_502) // Bad Gateway
            503 -> getString(R.string.error_code_503) // Service Unavailable
            504 -> getString(R.string.error_code_504) // Http Version Not Supported
            else -> getString(R.string.error_default)
        }
    } else {
        return getString(R.string.error_code_504)
    }
}